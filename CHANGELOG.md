# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
Quick remainder of the possible sections:
-----------------------------------------
Added, for new features.
Changed, for changes in existing functionality.
Deprecated, for soon-to-be removed features.
Removed, for now removed features.
Fixed, for any bug fixes.
Security, in case of vulnerabilities.
Maintenance, in case of rework, dependencies change

Also, we can try to be consistent on the "what changed":
--------------------------------------------------------
- **CSS** for general stylesheets changes (resets, helpers, ...)
- **Components** for changes that directly impact components
- **Documentation system** for all the changes made to the documentation _system_
- **Library** for changes to the JS parts of the library: options, helpers, etc...
- **Dependencies** for changes on the JS/CSS dependencies
-->

## [Unreleased]

## [2.1.1] 06/12/2024

### Changed

**Components**

- `RuiButton`: Changed the click behavior: only block actions when button is disabled instead of doing it every time.

## [2.1.0] 07/11/2024

### Changed

**CSS**

- `_timeline` styles were reworked to be a bit more compact
- `vue-multiselect` now have the same "arrow" as the `select` element
- Fix ugly font rendering on WebKit
- Workaround color input rendering on WebKit
- Use available variables for all font weights

**Components**

- `RuiThumbnails`: Remove padding

### Fixed

**CSS**

- `_timeline` icon now uses a good color in both light/dark themes
- Arrow in `select` inputs don't overlap text when displayed inline

## [2.0.0] 29/10/2024

### Added

**CSS**

- Added missing `-text--xlarge` and `-text--xxlarge` helpers.
- Added new CSS variables for negative spacers : `--spacer-*-n`.
- Added `-flex-column` and `-inline-flex-column` helpers
- Completed `-h*` helpers: `-h1` to `-h6` are now available to look like their corresponding header.

### Changed

**CSS**

- Most of the size units are now in `rem` unit. Change the `--font-size-base` variable to adapt all the components.

### Fixed

**CSS**

- Reworked reset for `<select>` to be better displayed in WebKit browsers
- Changed the orange color for something brighter

**Components**

- `RuiModal`: Fix footer height on "large" variant
- `RuiDropdown`: Fix the case when clicking on a menu entry makes the component disappear (expectedly), leading to console
  errors.
- `RuiMenuItem`: Only display a cursor pointer when item is a link or a button

## [2.0.0-alpha.1] 19/10/2024 - Rework the bundle

### Added

**Components**

- `RuiTag`: Added `color` prop that accepts hexadecimal value (transparency not supported)

**Library**

- New CSS files variants are available in `dist` directory.

### Changed

**CSS**

- Slightly changed `mute` color and its alternative color to better fit with other states

**Components**

- [BREAKING] `RuiToolbar`: Removed the `help` prop and slot. It's replaced by a `right` slot only, without integration of the
  `RuiHelpButton` to minimize dependency on external libraries.

**Library**

- [BREAKING] The library stylesheet is no more included with the `RiseUI` plugin, you will need to import it yourself.
- [BREAKING] The library stylesheet has been split into different flavors:
  - `minimal.scss`: only the reset, helpers and theme colors. Perfect to build your own components
  - `with_css_components.scss`: ...well... the minimal set _plus_ the few CSS-only components
  - `rise_ui.scss`: Everything above _plus_ the VueJS component styles
  - `full.scss`: Everything above plus the styles for the supported external libraries. You will need these libs for the
    styles to work.
  - `fonts.scss`: The font declaration. Extracted from the styles so it can be changed without having unused fonts around.
- [BREAKING] The components relying on external libraries are removed from the components index: `RuiSearchForm`,
  `RuiHelpButton`, `RuiHelpButtonContent`, `RuiRgbaPicker` and `RuiTagSelector`.
- [BREAKING] `RiseUI` plugin won't load all the components anymore. Import them individually in your
  components/views, or import them globally in your entrypoint.
- [BREAKING] JS files are not compiled anymore. Only CSS files are:
  - `dist/minimal.scss`
  - `dist/with_css_components.scss`
  - `dist/rise_ui.scss`
  - `dist/full.scss`
  - `dist/fonts.scss`
- [BREAKING] Fonts declarations are extracted from the SCSS file for more control and smaller generated CSS files.
- [BREAKING] Rework of library exports (paths that bundlers - at least Vite - will follow when importing files).

**Documentation system**

- [BREAKING] `DocumentationSystem` plugin won't load all the documentation system components anymore. Import them
  individually in your documentation components/views or import them globally in your documentation entrypoint.

### Fixed

**CSS**

- Improved styling of _color_ inputs

**Components**

- `RuiItem`: Use correct color for disabled state

## [2.0.0-alpha] 15/10/2024

### Added

**CSS**

- Add text helper `-text--nowrap` which prevents line breaks
- Added new colors and alternatives colors as CSS variables (see the docs for "CSS variables > Colors")
- Created new `-bg--color-*` variants for new colors
- Created new `-text--color-*` variants for new colors
- Added `-text--<state>-alt` variants for state colors
- Added `-bg--color-<state>-alt` and `-bg--color-<state>-alt` helpers
- Added `-b*`, `-b*--alt`, `-b*--none` border helpers
- Added helpers for flexboxes and grids:
  - `-flex-none`
  - `-align-self-start`,
  - `-align-items-start`, `-align-items-baseline`,
  - `-justify-content-start`, `-justify-content-center`, `-justify-content-right`, `-justify-content-left`
- Added `-grid-1-3` helper
- Added new visibility helpers:
  - `-inline`, `-sm:inline`, `-md:inline`, ...
  - `-inline-block`, `-sm:inline-block`, `-md:inline-block`, ...
- Added `-text--weight-*` helpers for font weights
- Added `-link--text` helper to display links like text
- Added breakpoints CSS variables: `--bp-small`, `--bp-medium` and `--bp-large`
- Added more input-related CSS variables: `--input-placeholder-opacity` and `--input-font-size`

**Components**

- `RuiEmpty`: Added an `actions` slot to display optional buttons
- `RuiSearchForm`: Emits `submit` on form submission
- `RuiSearchForm`: Add a default placeholder
- `RuiSearchForm`: Add a button to clear input
- `RuiSearchForm`: Add a `submittable` prop to control display of the submit button (false by default)
- `RuiDateTimePicker`: Add all props applicable to the `RuiField` wrapper
- `RuiDateTimePicker`: Add `clearable` prop: adds a button to clear the field
- `RuiDropdown`: Added `arrowIcon` prop to replace the arrow on the trigger
- `RuiDropdown`: Add `variant` prop to change trigger color. Uses the same variants as the `RuiButton`
- `RuiButton`: Add `*-text` variants. It displays a normal button with the colored text only.
- `RuiErrors`: Add a slot for additional content or actions

**Documentation system**

- Catch errors when generating metadata
- Add support for `@propsGroup` tag, usable on component definition to group props together in API and Playground pages
- Add support for a list of components to ignore

### Changed

**CSS**

- Slightly decreased font size in `pre` blocks
- [BREAKING] Rename all `-text--<color>` helpers to `-text--color-<color>`.
- [BREAKING] Rename `-flex--center` by `-justify-content-center`
  [BREAKING] Rename `-flex--right` by `-justify-content-right`
- Desaturate inputs backgrounds on dark theme
- Rename variable `--input-color-placeholder` to `--input-placeholder-color`

**Components**

- `RuiListItem`: Remove paddings
- `RuiEmpty`: Change default icon
- [BREAKING] `RuiFormErrors`: Renamed component to `RuiErrors`
- [BREAKING] `RuiCollapsibleListItem`: Component now uses `details` tag; its template has been simplified, as its CSS classes.
- `RuiField`: Handle arrays of errors
- [BREAKING] Replaced deprecated prop `hasFooter` on `RuiModal` by its inverse `noFooter` which defaults to `false`.
- `RuiToolbar`: Remove extra padding in buttons
- `RuiErrors`: Decrease title size
- `RuiTagSelector`: Rework CSS of the whole component to make it more compact and complete some missing overrides.

### Removed

**Library**

- Remove previously deprecated `randomString()` from `/path/to/RandomString.js`. Change import to the same paths, but
  to `String.js` instead.

**Dependencies**
- [BREAKING] Removed "toaster-js" dependency as it only was used for the styles overrides. If you don't use toaster-js,
  remove the dependency; If you used it and the RiseUI CSS copy and use the overrides
  [from the previous version](https://gitlab.com/experimentslabs/garden-party/rise-ui/-/blob/f5a26f322560c1bdca911431ea89ed36dee6862c/src/stylesheets/lib/_toast-js.scss).

**CSS**

- Remove previously deprecated `-grid--gap` an `-flex--gap` helpers

### Fixed

**CSS**

- `code`: Prevent wrapping
- `-grid-*` helpers now have a minimum width, preventing some rendering issues
- Fixed display of Rails UJS "button_to" in `RuiField` appended controls
- `_writing`: Remove top margin on first paragraph, bottom margin on the last

**Components**

- `RuiLoading`: Use the right animation (animation from a dependency was used by error)
- `RuiTag`: Prevent wrapping
- `RuiIcon`: Prevent icon to resize in flexboxes
- `RuiButton`: Prevent wrapping
- `RuiIconText`: Prevent icon to downsize
- Active `RuiButton`'s background changed to be more distinguishable from other components
- `RuiButton`: Remove icon margin on small size

**Documentation system**

- Improved display of prop controls
- Improve markdown renderer component layout for long lines
- `@preset` names now support any character except `{`
- Separate allowed values with a comma in the API view
- Remove dependency to `vue-i18n` in documentation. If you use the documentation system in your project but not `vue-i18n`,
  you can now remove it.

## [1.3.0] 28/07/2024 - RuiDropdown improvements

### Added

**Components**

- `RuiDropdown`: Added `icon` prop to prepend an icon on the trigger
- `RuiDropdown`: Added `noCaption` prop. It will hide the caption and caret on the right, only displaying the icon. If
  the `caption` prop is provided, it will use it as `title` on the trigger. Does not work with the `caption` slot.
- `RuiDropdown`: Added `noTextOnSmall` prop to hide caption on small screens
- `RuiDropdown`: Close menu when clicking outside
- `RuiDropdown`: Close menu when clicking on entry

### Fixed

**Components**

- `RuiBadge`: Prevent line breaks
- `RuiNavbarMenu`: Use a button as trigger for small views

**CSS**

- Make `pre` scroll horizontally if needed

### Maintenance

**Dependencies**

- Updated dependencies with a major version change. No change to the code was required
  - Updated peer dependency `debounce` to v2.1.0
  - Updated peer dependency `floating-vue` to v5.2.2
- Updated other dependencies

## [1.2.0] 09/05/2024 - Helpers, more reset tags, fixes and small improvements

### Added

**Library:**

- Added `src/lib/String.js` with string helpers

**CSS**

- Style `kbd` tags
- Style `fieldset` and `legend` tags
- Add background helpers: `-bg-component`, `-bg-component--alt`, `-bg-default` and `-bg-default--alt`
- Add flex/grid helper: `-align-items-end` and `-gap`
- Add position helper: `-position-relative`
- Add `-text--struck` helper

**Documentation system:**

- Added `ContentWithToc.vue` to render a table of content beside some content. Table of content is generated using
  content's `h*` tags.

### Changed

**Library:**

- Reworked `randomString()` and added it to the string helpers

**CSS:**

- Make labels smaller
- Rework font sizes

**Components:**

- `RuiPanel`: Change resizer look and feel

### Deprecated

**Library:**

- Deprecated `randomString()` from `src/lib/randomString`. Use `randomString` from `src/lib/String.js`

### Removed

**CSS:**

- Deprecated `-flex--gap` and `-grid--gap`. Use `-gap` instead

### Fixed

**Components:**
- `RuiPanel`: Prevent text selection when resizing
- `RuiForm`: Remove erroneous and useless style tag at the beginning of the file

### Maintenance

- Updated dependencies to their next patch

## [1.1.0] 14/01/2024 - Documentation system enhancements

### Added

**Documentation system**

- Added support for `@playgroundMinHeight` tag: any string that can be used in a `min-height` CSS attribute.
- Added support for `@playgroundBackground` tag. Only "checkboard" is supported for now.
- Added support for `@see` tags in components documentation.
- Added `doc-playground` CSS class to ease CSS overrides when playground tags are not enough.

## [1.0.0] 31/12/2023 - Let's go

The project works well enough to be published and used.

### Added

Components:

- `RuiAlert`: Added `content` prop.

Documentation system:

- `MarkdownRenderer`: Added `brokenLinks` prop to display a warning about possible broken links in the file.

## [0.1.1] 2023-12-29 - Quick fix

### Fixed

CSS:

- `RuiHelpButton`: style from `floating-vue` is moved to `stylesheets/lib` to allow usage of RiseUI without it.

## [0.1.0] 2023-12-25 - Getting closer to something mature

### Added

Components:

- `RuiButton`: Add the `waiting` prop which disables the button and changes the cursor
- `RuiForm`: Add the `fixedActions` prop to make the actions area non-sticky

CSS:

- Added all possible variants of paddings and margins (`-m*`, `-mv*`, ...)

Documentation system:

- Adds a class to manage local storage entries, corresponding VueJS plugin and make an instance available via
  `this.$storage` in components. You can use it like:
  ```js
  this.$storage.write('key', value) // value must be serializable
  this.$storage.read('key')         // returns value
  this.$storage.addAccessor('cats') // Creates getter and setter for "cats":
  this.$storage.cats = ['Boris', 'Jean Michel']
  this.$storage.cats                // returns ['Boris', 'Jean Michel']
  ```
  A getter and setter for `theme` is set by default by the documentation system.

### Changed

Components:

- `RuiSubmitButton`: change cursor when loading
- `RuiListItem`: Don't always display a pointer cursor
- `RuiCollapsibleListItem`: Use a `button` element as trigger instead of a clickable `div`

CSS:

- Inputs backgrounds were reworked to be less aggressive to the eye (a personal opinion...)

## Deprecated

Components:

- `RuiModal`: Deprecated `hasFooter` prop : it's clunky and usage feels weird.

### Fixed

Components:

- `RuiDateTimePicker`: Don't reset date when typing a new year
- `RuiImageViewer`:
  - Don't display arrows when there is only one picture
  - Improve visibility of the "close" button
- `RuiButton`: Display the right cursor on disabled buttons/links
- `RuiDropdown`: Display the right cursor when hovering the trigger
- `RuiHelpButton`: Display the right cursor when hovering the trigger
- Improve contrast ratio for texts in:
  - `RuiEmpty`
  - `RuiItem`
  - `RuiButton`

CSS:

- Radios and checkboxes:
  - Make radio inputs slightly smaller
  - Improve contrast of checked radio and checkbox inputs
- Improve contrast of buttons and their different states
- Improve visibility of focused elements
- Fixed sub-list numbering for Firefox (`ol` in `ol`)

Documentation system:

- Fix example name with multiple separators
- Fix navigation between HTML and VueJS examples not working
- Fixed contrast of highlighted code blocks (both themes)
- Fixed CSS class of active page in "Components" menu

### Maintenance

- [BREAKING] `vue3-popper` optional dependency has been replaced by `floating vue` because of unexpected behavior and lack of
  updates of the library. **This is a breaking change** and you should replace `vue3-popper` in your dependencies by
  `floating-vue` (or add `floating-vue` along `vue3-popper` if you use the latter for your own components).
- [BREAKING] Node 20 is now used in development.
- `debounce` has been upgraded to version 2
- `@fontsource/ubuntu` has been upgraded to version 5

## [0.0.6] 2023-12-05 - Documentation system

Same as 0.0.5, with all the documentation system files.

## [0.0.5] 2023-12-05 - Documentation system

#### Added

Documentation:

- Big rework has been done to be able to use the documentation system to document projects using RiseUI

CSS:

- Create `_writing` HTML component

## [0.0.4] 2023-10-11 - Fixes and improvements

### Added

Components:

- `RuiIcon` Specify alternative pack with a prefix
- `RuiMenuSectionTitle`: Create new component
- `RuiNavbar`: Add `shadowed` prop for a light shadow under the navbar
- `RuiNavbar`: Add `sticky` prop

Library:

- Added `iconsFiles` configuration object to declare custom additional icons files

CSS:

- Add reset for `blockquote` tags
- Add `-mld` and `-mrd` margin helpers
- Add `-pll`padding helper

### Changed

Components:

- `RuiDropdown` Use `details` tag. Dropdown now opens on click.

CSS:

- `.rui-dropdown` is now meant to be used with `details` and `summary` tags

### Fixed

Library:

- Make fallback `$t` method handle translation object with sub-keys instead of dot-separated paths only

CSS:

- Target last elements of toolbars more precisely
- Remove horizontal padding for navbars in `_container`
- Add space above fields in grids
- Fix text vertical alignment in tags
- `.rui-icon-text`: Simplify style

Components:

- `RuiNavbar`: Improve usage of toolbars and dropdown in menus
- `RuiButton`: Simplify template

## [0.0.3] 2023-09-19 - Small changes, new documentation system

### Added

Library:

- Expose components to import them individually: `ìmport {RuiButton, RuiIcon} from '@experiments-labs/rise_ui/components'`

Components:

- `RuiTimeOutButton`: Add an icon visible when timer is on

Documentation:

- Replaced Storybook by a handcrafted documentation solution.

## [0.0.2] 2023-09-14 - Fixes and improvements

### Added

CSS:

- Themes can also be applied on a specific element with the `.rui` class: `.rui.rui-theme--dark` or `.rui.rui-theme--light`
- `pre` tags now have a reset style
- `del` tags now have a reset style
- `.grid-1-4` variant

Components:

- `RuiMenuItem` now has an `active` prop
- `RuiIconText` now has a `tag` prop to change the wrapping tag

Library:

- Export `translations` object. Import them with `import {translations} from '@experiments-labs/rise_ui'`

### Changed

CSS:

- `.sr-only` is renamed to `.-sr-only`
- `.-*-sr-only` variants are renamed to `-*:sr-only`
- `.-grid--*` variants are renamed to `.-grid-*` (e.g.: `.-grid--2` becomes `.-grid-2`)
- `.-grid-gap` is renamed to `-grid--gap`
- `.-flex-*` variants are renamed to `-flex--*`
- `.-center` is renamed to `.-text--center`
- `.-left` is renamed to `.-text--left`
- `.-right` is renamed to `.-text--right`
- `.-smaller` is renamed to `.-text--smaller`
- `.-small` is renamed to `.-text--small`
- `.-default-size` is renamed to `.-text--default-size`
- `.-medium` is renamed to `.-text--medium`
- `.-large` is renamed to `.-text--large`
- `.-monospace` is renamed to `.-text--monospace`
- `.-italic` is renamed to `.-text--italic`
- `.-ellipsis` is renamed to `.-text--ellipsis`
- `.-ok` is renamed to `.-text--ok`
- `.-bad` is renamed to `.-text--bad`
- `rui-icon` sizes have been improved

Components:

- `RuiToolbarsSpring` was moved and renamed to `RuiToolbarSpring`
- `RuiNavbar` caption is now optional
- `RuiBreadcrumbs` template has been simplified
- Fix renamed class names, missed during import

JS utils:

- `dateToInputString(date)` now handles strings as input (and converts it to a `Date`)

I18n:

- Translation objects are keyed under `<locale>.rui.*` instead of `<locale>.*`

### Removed

CSS:

- `.visually-hidden`

### Fixed

CSS:

- `._timeline`: Fix icon position

Components:

- `FormErrors` is renamed `RuiFormErrors` internally
- `DateTimePicker` is renamed `RuiDateTimePicker` internally
- `RuiField`: prevent line wrapping on appended elements
- `RuiDrawer`: Fix left and right drawer position
- `RuiToolbar`: Better target buttons in CSS
- `RuiDropdown`: Fix corners when used in toolbars
- `RuiNavbar`: Align items vertically
- `RuiTags`: Fix composed tag display

## [0.0.1] 2023-08-19 - Base import

### Added

- Create a VueJS components library
- Import components from GardenParty project
  - Vue components are prefixed with `Rui` instead of `Gp`
  - CSS classes are prefixed with `rui-` instead of `gp-`
  - Some components were not yet imported

### Maintenance

- Create project with `vitest`, `prettier`, and `eslint`, licensed under the MIT license
- Create `.editorconfig` file
- Create `.node-version` with Node 16 as target. Node 18 is the current LTS but Vue dependency has SSL issues with it currently.
- Add ESLint plugins and rules
- Add Stylelint plugins and rules
- Add Storybook and configure it
- Adapt tests
