# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
Quick remainder of the possible sections:
-----------------------------------------
Added, for new features.
Changed, for changes in existing functionality.
Deprecated, for soon-to-be removed features.
Removed, for now removed features.
Fixed, for any bug fixes.
Security, in case of vulnerabilities.
Maintenance, in case of rework, dependencies change
-->

## [Unreleased]

### Added

Library:

- Expose components to import them individually: `ìmport {RuiButton, RuiIcon} from '@experiments-labs/rise_ui/components'`

Components:

- `RuiTimeOutButton`: Add an icon visible when timer is on

Documentation:

- Replaced Storybook by a handcrafted documentation solution.

## [0.0.1] 2023-09-14 - Fixes and improvements

### Added

CSS:

- Themes can also be applied on a specific element with the `.rui` class: `.rui.rui-theme--dark` or `.rui.rui-theme--light`
- `pre` tags now have a reset style
- `del` tags now have a reset style
- `.grid-1-4` variant

Components:

- `RuiMenuItem` now has an `active` prop
- `RuiIconText` now has a `tag` prop to change the wrapping tag

Library:

- Export `translations` object. Import them with `import {translations} from '@experiments-labs/rise_ui'`

### Changed

CSS:

- `.sr-only` is renamed to `.-sr-only`
- `.-*-sr-only` variants are renamed to `-*:sr-only`
- `.-grid--*` variants are renamed to `.-grid-*` (e.g.: `.-grid--2` becomes `.-grid-2`)
- `.-grid-gap` is renamed to `-grid--gap`
- `.-flex-*` variants are renamed to `-flex--*`
- `.-center` is renamed to `.-text--center`
- `.-left` is renamed to `.-text--left`
- `.-right` is renamed to `.-text--right`
- `.-smaller` is renamed to `.-text--smaller`
- `.-small` is renamed to `.-text--small`
- `.-default-size` is renamed to `.-text--default-size`
- `.-medium` is renamed to `.-text--medium`
- `.-large` is renamed to `.-text--large`
- `.-monospace` is renamed to `.-text--monospace`
- `.-italic` is renamed to `.-text--italic`
- `.-ellipsis` is renamed to `.-text--ellipsis`
- `.-ok` is renamed to `.-text--ok`
- `.-bad` is renamed to `.-text--bad`
- `rui-icon` sizes have been improved

Components:

- `RuiToolbarsSpring` was moved and renamed to `RuiToolbarSpring`
- `RuiNavbar` caption is now optional
- `RuiBreadcrumbs` template has been simplified
- Fix renamed class names, missed during import

JS utils:

- `dateToInputString(date)` now handles strings as input (and converts it to a `Date`)

I18n:

- Translation objects are keyed under `<locale>.rui.*` instead of `<locale>.*`

### Removed

CSS:

- `.visually-hidden`

### Fixed

CSS:

- `._timeline`: Fix icon position

Components:

- `FormErrors` is renamed `RuiFormErrors` internally
- `DateTimePicker` is renamed `RuiDateTimePicker` internally
- `RuiField`: prevent line wrapping on appended elements
- `RuiDrawer`: Fix left and right drawer position
- `RuiToolbar`: Better target buttons in CSS
- `RuiDropdown`: Fix corners when used in toolbars
- `RuiNavbar`: Align items vertically
- `RuiTags`: Fix composed tag display

## [0.0.1] 2023-08-19 - Base import

### Added

- Create a VueJS components library
- Import components from GardenParty project
  - Vue components are prefixed with `Rui` instead of `Gp`
  - CSS classes are prefixed with `rui-` instead of `gp-`
  - Some components were not yet imported

### Maintenance

- Create project with `vitest`, `prettier`, and `eslint`, licensed under the MIT license
- Create `.editorconfig` file
- Create `.node-version` with Node 16 as target. Node 18 is the current LTS but Vue dependency has SSL issues with it currently.
- Add ESLint plugins and rules
- Add Stylelint plugins and rules
- Add Storybook and configure it
- Adapt tests
