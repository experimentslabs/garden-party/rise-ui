import { Ls } from '../utils/LS.js'

/**
 *  @typedef {import('vue').App} VueApp
 */
export const LocalStorage = {
  /**
   * Initializes localStorage helper
   *
   * @param   {VueApp}           app               - Vue application
   * @param   {{prefix: string}} options           - Instance options
   * @param   {string}           options.prefix    - Prefix for the entries
   * @param   {string[]}         options.accessors - List of pre-defined accessors
   * @returns {VueApp}                             Vue application
   */
  install (app, { prefix, accessors = [] }) {
    app.config.globalProperties.$storage = new Ls(prefix, accessors)

    return app
  },
}
