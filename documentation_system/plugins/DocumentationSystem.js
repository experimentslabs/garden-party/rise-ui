import { createRouter, createWebHistory } from 'vue-router'

// Other plugins
import { LocalStorage } from './LocalStorage.js'

// RiseUI
import { RiseUI } from '../../src/library.js'

// Documentation system
import { DocumentationLibrary } from '../utils/Documentation.js'
import '../stylesheets/styles.scss'
import icons from '../assets/images/icons.svg'
import { underscore } from '../utils/String.js'

/**
 *  @typedef {import('../../../src/library.js').RiseUiOptions} RiseUiOptions
 *  @typedef {import('../menu.js').MenuDefinition} MenuDefinition
 *  @typedef {import('../config.js').ProjectDefinition} ProjectDefinition
 *  @typedef {import('vue').App} VueApp
 */

/**
 * @typedef {object} DocumentationSystemOptions
 * @property {Function}           loadHTMLExample       - Method to load HTML example
 * @property {Function}           loadVueExample        - Method to load VueJS example
 * @property {Function}           loadVueExampleSource  - Method to load VueJS example source code
 * @property {Function}           loadMarkdownFile      - Method to load Markdown file
 * @property {object}             documentation         - Generated documentation
 * @property {string[]}           localStorageAccessors - List of accessors for the local storage plugin. "theme" is already set.
 * @property {?object[]}          routes                - Documentation routes list
 * @property {?MenuDefinition}    menu                  - Menu entries
 * @property {?ProjectDefinition} project               - Project configuration
 * @property {?RiseUiOptions}     riseUiOptions         - RiseUI options
 */

export const DocumentationSystem = {
  /**
   * Initializes the documentation system
   *
   * @param   {VueApp}                     app     - Vue application
   * @param   {DocumentationSystemOptions} options - Documentation options
   * @returns {VueApp}                             Vue application
   */
  install (app, {
    loadHTMLExample,
    loadVueExample,
    loadVueExampleSource,
    loadMarkdownFile,
    documentation,
    localStorageAccessors = [],
    routes = null,
    menu = [],
    project = { repositoryUrl: null, version: null, name: 'Components documentation' },
    riseUiOptions = {},
  }) {
    if (typeof loadHTMLExample !== 'function') throw new Error('"loadHTMLExample" method is missing')
    if (typeof loadVueExample !== 'function') throw new Error('"loadVueExample" method is missing')
    if (typeof loadVueExampleSource !== 'function') throw new Error('"loadVueExampleSource" method is missing')
    if (typeof loadMarkdownFile !== 'function') throw new Error('"loadMarkdownFile" method is missing')

    // RiseUI
    if (!riseUiOptions.iconsPacks) riseUiOptions.iconsPacks = {}
    if (!riseUiOptions.iconsPacks.doc) riseUiOptions.iconsPacks.doc = icons
    app.use(RiseUI, riseUiOptions)

    // Router
    app.use(createRouter({
      history: createWebHistory(import.meta.env.BASE_URL),
      routes: [
        ...routes,
        {
          path: '/docs/components/:component',
          component: () => import('../views/DocumentView.vue'),
          children: [
            { name: 'doc-api', path: 'api', component: () => import('../views/Component/ComponentApi.vue') },
            { name: 'doc-playground', path: 'playground', component: () => import('../views/Component/ComponentPlayground.vue') },
            { name: 'doc-vue', path: 'vue', component: () => import('../views/Component/ComponentExamples.vue'), meta: { fileType: 'vue' } },
            { name: 'doc-html', path: 'html', component: () => import('../views/Component/ComponentExamples.vue'), meta: { fileType: 'html' } },
          ],
        },

        { path: '/pages/:page', name: 'markdown-page', component: () => import('../views/MarkdownPageView.vue') },
        { path: '/:pathMatch(.*)*', name: '404error', component: () => import('../views/404View.vue') },
      ],
    }))

    // LocalStorage helper
    const prefix = project.name || 'another_documentation_system'
    if (localStorageAccessors.findIndex((a) => a === 'theme') === -1) localStorageAccessors.push('theme')
    app.use(LocalStorage, {
      prefix: underscore(prefix),
      accessors: localStorageAccessors,
    })

    // Main menu
    app.config.globalProperties.$mainMenu = menu

    // Configuration
    app.config.globalProperties.$project = project

    // Page title plugin
    app.config.globalProperties.$pageTitle = project.name
    app.config.globalProperties.$setTitle = (string) => {
      document.querySelector('head > title').innerHTML = `${string} - ${project.name}`
      app.config.globalProperties.$pageTitle = string
    }

    // Documentation object plugin
    app.config.globalProperties.$docs = new DocumentationLibrary(documentation)
    app.config.globalProperties.$getDoc = function (name) { return this.$docs.getComponent(name) }
    app.config.globalProperties.$getExample = function (name) { return this.$docs.getExample(name) }

    app.config.globalProperties.$loadHTMLExample = loadHTMLExample
    app.config.globalProperties.$loadVueExample = loadVueExample
    app.config.globalProperties.$loadVueExampleSource = loadVueExampleSource
    app.config.globalProperties.$loadMarkdownFile = loadMarkdownFile

    return app
  },
}
