import { defineComponent } from 'vue'
import DOMPurify from 'dompurify'

export default defineComponent({
  name: 'DocVHtml',
  directives: {
    swap: {
      mounted (el, binding) {
        const safe = DOMPurify.sanitize(binding.value)

        const frag = document.createRange().createContextualFragment(safe)
        el.replaceWith(frag)
      },
    },
  },
  props: {
    html: { required: true, type: String },
  },
  template: '<div v-swap="html"></div>',
})
