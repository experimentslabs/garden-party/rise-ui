/**
 * Replace anything that is not a letter or digit by an underscore, and lower the string casing
 *
 * @param   {string} string - String to transform
 * @returns {string}        Transformed string
 */
export function underscore (string) {
  return string.replace(/[^a-zA-Z0-9]/g, '_').replace(/_{2,}/g, '_').toLowerCase()
}
