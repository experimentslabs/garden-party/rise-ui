import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest'

import { Ls } from './LS.js'

describe('Ls', () => {
  /** @type {Ls} */
  let instance

  beforeEach(() => {
    instance = new Ls('test', ['dogs'])
  })

  afterEach(() => {
    localStorage.clear()
  })

  describe('new', () => {
    it('creates accessors', () => {
      expect(instance).toHaveProperty('dogs')
    })
  })

  describe('.addAccessor', () => {
    it('adds an accessor', () => {
      expect(instance).not.toHaveProperty('cats')
      instance.addAccessor('cats')
      expect(instance).toHaveProperty('cats')
    })
  })

  describe('.write', () => {
    it('saves the value', () => {
      const setItemSpy = vi.spyOn(Storage.prototype, 'setItem')
      instance.write('entry', 'value')

      expect(setItemSpy).toHaveBeenCalledWith('test_entry', '"value"')
    })
  })

  describe('.read', () => {
    describe('basic behavior', () => {
      it('retrieve the value', () => {
        const getItemSpy = vi.spyOn(Storage.prototype, 'getItem')
        instance.read('entry')

        expect(getItemSpy).toHaveBeenCalledWith('test_entry')
      })

      it('returns the stored value', () => {
        instance.write('entry', 'value')

        expect(instance.read('entry')).toEqual('value')
      })
    })

    describe.each([
      /* eslint-disable no-multi-spaces, array-bracket-spacing */
      ['number (int)',    1,             1             ],
      ['number (float)',  1.1,           1.1           ],
      ['string',          'hello world', 'hello world' ],
      ['boolean (true)',  true,          true          ],
      ['boolean (false)', false,          false         ],
      /* eslint-enable no-multi-spaces, array-bracket-spacing */
    ])('reads and write %s', (type, input, expected) => {
      it(`returns a ${type}`, () => {
        instance.write('data', input)

        expect(instance.read('data')).toStrictEqual(expected)
      })
    })
  })

  describe('accessors', () => {
    it('reads and write data', () => {
      instance.addAccessor('cats')
      const value = ['Sherlock', 'Watson']
      instance.cats = value
      expect(instance.cats).toStrictEqual(value)
    })
  })
})
