/**
 * Checks if the clipboard API is supported
 *
 * @returns {boolean} Availability status
 */
export function clipboardCopySupported () {
  return !!(navigator && navigator.clipboard && navigator.clipboard.writeText)
}

/**
 * Copies text to clipboard
 *
 * @param   {string}        content - Text to copy
 * @returns {Promise<void>}         Resolved promise
 */
export async function copyToClipboard (content) {
  await navigator.clipboard.writeText(content)
}
