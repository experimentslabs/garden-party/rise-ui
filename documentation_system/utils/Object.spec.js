import { describe, it, expect } from 'vitest'

import { flattenDict } from './Object.js'

describe('obj.flattenDict', () => {
  it('returns object with dotted paths', () => {
    const input = {
      a: {
        aa: {
          aa1: 'aa1',
          aa2: 'aa2',
        },
        ab: {
          ab1: 'ab2',
          ab2: [
            'a',
            1,
            true,
            null,
          ],
        },
      },
      b: 'b.str',
      c: {
        c1: 'c1',
        c2: 'c2',
      },
    }
    const expected = {
      'a.aa.aa1': 'aa1',
      'a.aa.aa2': 'aa2',
      'a.ab.ab1': 'ab2',
      'a.ab.ab2.0': 'a',
      'a.ab.ab2.1': 1,
      'a.ab.ab2.2': true,
      'a.ab.ab2.3': null,
      b: 'b.str',
      'c.c1': 'c1',
      'c.c2': 'c2',
    }

    expect(flattenDict(input)).toStrictEqual(expected)
  })
})
