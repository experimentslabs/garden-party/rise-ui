import path from 'path'
import { globSync } from 'glob'
import fs from 'fs'
import VueDocgenApi from 'vue-docgen-api'

import { Metadata } from './Metadata.js'
import { Logger } from './Logger.js'

/**
 * Extracts documentation from components
 *
 * @param   {string}          docsAppPath            - Absolute path to documentation app root (not "src")
 * @param   {string}          relativeComponentsPath - Path to the directory containing the components to document, relative to docsAppPath
 * @returns {Promise<object>}                        Documentation for every component
 */
async function extractDocumentation (docsAppPath, relativeComponentsPath) {
  const componentsPath = path.resolve(docsAppPath, relativeComponentsPath)
  const pattern = `${componentsPath}/**/*.vue`
  const files = globSync(pattern)

  Logger.info(`Searching components in ${pattern}`)

  const promises = []

  const list = []
  files.forEach((file) => {
    const promise = VueDocgenApi.parse(file)
      .then((docs) => {
        // Replace absolute path
        docs.sourceFiles = docs.sourceFiles.map(f => f.replace(`${componentsPath}/`, `../${relativeComponentsPath}`))

        list.push(docs)
      })
      .catch((error) => {
        const message = [
          'An error happened during extraction of:',
          `  ${file}`,
          'The file will be ignored.\n',
          'For information, here is the original error:',
          error,
        ].join('\n')
        Logger.error(message)
      })

    promises.push(promise)
  })

  await Promise.all(promises)

  return list
}

/**
 * Lists all documentation examples
 *
 * @param   {string}          docsAppPath - Absolute path to documentation app root (not "src")
 * @returns {Promise<object>}             List of examples per component
 */
async function listExamples (docsAppPath) {
  const pattern = `${docsAppPath}/src/examples/**/*.{vue,html}`
  const regex = new RegExp(`^${docsAppPath}/src/examples/`)

  Logger.info(`Searching examples in ${pattern}`)

  const list = {}
  globSync(pattern)
    .sort()
    .forEach((f) => {
      const filePath = f.replace(regex, '')
      const chunks = filePath.split(path.sep)
      const component = chunks.shift()
      if (!list[component]) list[component] = { html: [], vue: [] }

      const exampleType = filePath.match(/\.vue$/) ? 'vue' : 'html'

      list[component][exampleType].push(filePath.replace(/\.(vue|html)$/, ''))
    })

  return list
}

/**
 * Generate documentation metadata from examples and VueJS components
 *
 * @param   {object}        paths                        - Various paths
 * @param   {string}        paths.docsAppPath            - Absolute path to documentation app root (not "src")
 * @param   {string}        paths.relativeComponentsPath - Path to the directory containing the components to document, relative to docsAppPath
 * @param   {string[]}      paths.ignoredComponents      - List of components to ignore
 *
 * @returns {Promise<void>}
 */
export async function generateMetadata ({ docsAppPath, relativeComponentsPath, ignoredComponents = [] }) {
  const docs = await extractDocumentation(docsAppPath, relativeComponentsPath)
  const examples = await listExamples(docsAppPath)

  const output = Metadata.mix(docs, examples, ignoredComponents)

  Metadata.check(output)

  fs.writeFileSync(path.resolve(docsAppPath, 'src/documentation.json'), JSON.stringify(output))
}
