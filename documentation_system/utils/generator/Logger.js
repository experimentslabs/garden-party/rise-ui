import { C } from './C.js'

export class Logger {
  static info (message) { this._display(C.blue(message)) }
  static warn (message) { this._display(C.brown(message)) }
  static error (message) { this._display(C.red(message)) }
  static notice (message) { this._display(C.blue(message)) }

  static _display (message) { console.log(message) } // eslint-disable-line no-console
}
