const colors = {
  black: [30, 40],
  red: [31, 41],
  green: [32, 42],
  brown: [33, 43],
  blue: [34, 44],
  purple: [35, 45],
  cyan: [36, 46],
  lightGray: [37, 47],
}

/**
 * Colorize a string for console output
 *
 * @param   {string} string - String to colorize
 * @param   {string} color  color name
 * @returns {string}        String to display
 */
function _colorize (string, color) { return `\x1b[${colors[color][0]}m${string}\x1b[0m` }

/**
 * Helper class to colorize strings
 *
 * @property {Function} black     - Colorizes string
 * @property {Function} red       - Colorizes string
 * @property {Function} green     - Colorizes string
 * @property {Function} brown     - Colorizes string
 * @property {Function} blue      - Colorizes string
 * @property {Function} purple    - Colorizes string
 * @property {Function} cyan      - Colorizes string
 * @property {Function} lightGray - Colorizes string
 */
export class C {
}

const methods = {}
Object.keys(colors).forEach(c => { methods[c] = (string) => _colorize(string, c) })
Object.assign(C, methods)
