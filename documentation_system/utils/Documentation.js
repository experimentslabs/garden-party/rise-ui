/**
 * Methods to fetch entries in metadata
 */
export class DocumentationLibrary {
  constructor (docs) {
    this._docs = docs
  }

  getComponent (name) {
    if (!Object.prototype.hasOwnProperty.call(this._docs, name)) throw new Error(`Unknown component ${name}`)

    return this._docs[name]
  }

  getExample (name) {
    return Object.keys(this._docs).forEach((k) => {
      const example = this._docs[k].examples[name]
      if (example) return example
    })
  }

  get componentNames () {
    return Object.keys(this._docs)
  }
}
