/**
 * Helper class to work with localStorage
 */
export class Ls {
  /**
   * Creates the instance
   *
   * @param {string}   prefix    - Prefix to use in keys to prevent collisions
   * @param {string[]} accessors - Optional list of accessors
   */
  constructor (prefix, accessors = []) {
    this._prefix = prefix

    accessors.forEach((accessor) => { this.addAccessor(accessor) })
  }

  /**
   * Creates getters/setters for a given attribute
   *
   * Example:
   * ```js
   * myLs.addAccessor('username')
   * // Creates:
   * myLs.username    // Getter
   * myLs.username=   // Setter
   * ```
   *
   * @param {string} name - Property name
   */
  addAccessor (name) {
    if (Object.prototype.hasOwnProperty.call(this, name)) throw new Error(`${name} property already exists`)

    Object.defineProperty(this, name, {
      get () { return this.read(name) },
      set (value) { this.write(name, value) },
    })
  }

  /**
   * Reads from the local storage
   *
   * @param   {string} key - Key to retrieve
   * @returns {any}        Stored value or null if invalid or not found
   */
  read (key) {
    let value = localStorage.getItem(`${this._prefix}_${key}`)
    try { value = JSON.parse(value) } catch (SyntaxError) { value = null }

    return value
  }

  /**
   * Writes to the local storage
   *
   * @param {string} key   - Storage key where to save data
   * @param {any}    value - Serializable value to store
   */
  write (key, value) { localStorage.setItem(`${this._prefix}_${key}`, JSON.stringify(value)) }
}
