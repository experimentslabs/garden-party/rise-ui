/**
 * Flattens an object to another object with key as dot-separated strings
 *
 * @param   {object}                 obj   - Object to flatten
 * @param   {string}                 path  - Optional path to start from
 * @param   {object}                 _list - Internal. List of entries for the current level
 *
 * @returns {{[key:string] :string}}       Flattened object
 */
export function flattenDict (obj, path = '', _list = {}) {
  Object.keys(obj).forEach((k) => {
    const currentPath = path === '' ? k : `${path}.${k}`

    if (obj[k] && typeof obj[k] === 'object') {
      Object.assign(_list, flattenDict(obj[k], currentPath, {}))
    } else {
      _list[currentPath] = obj[k]
    }
  })

  return _list
}
