import { describe, it, expect } from 'vitest'

import * as string from './String.js'

describe('string.underscore', () => {
  it('replaces characters by underscores', () => {
    expect(string.underscore('1%Hello./%World..9')).toEqual('1_hello_world_9')
  })
})
