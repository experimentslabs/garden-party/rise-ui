#!/bin/bash

# Creates an NPM release.
#
# Usage:
#  ./make_release.sh
# TARGET_DIR="somewhere/else" ./make_release
#

TARGET_DIR=${TARGET_DIR:=release}

ask() {
  read -r -p "$1 [y/N] " response
  response=${response,,}
  if [[ $response =~ ^(n| ) ]] || [[ -z $response ]]; then
    if [[ "" == "$2" ]]; then
      echo "Fix it then..."
    else
      echo "$2"
    fi
    exit 1
  fi
}

echo "Did you..."
ask "  - update the README"
ask "  - update the CHANGELOG"
ask "  - update the MIGRATION_GUIDE"
ask "  - bump version in package.json"
ask "  - have a successful CI pipeline"
ask "  - tag the version"

# Fail fast
set -xe

rm -rf "$TARGET_DIR"
mkdir "$TARGET_DIR"

yarn build:styles

mv dist "$TARGET_DIR"

for FILE in package.json src documentation_system CHANGELOG.md LICENCE README.md; do
  cp -r "$FILE" "$TARGET_DIR"
done

find "$TARGET_DIR" -type f -name "*.spec.js" -delete
find "$TARGET_DIR" -depth -type d -name "__snapshots__" -exec rm -rf {} \;

cd "$TARGET_DIR" || exit 1

set +xe
ask "Continue publication to NPM?" "Maybe later..."
set -xe

npm publish
