# Migration guide

## Next

## From 2.1.0 to 2.1.1

Nothing notable has been changed, but you can still read the [CHANGELOG](./CHANGELOG.md).

## From 2.0.0 to 2.1.0

Read the [CHANGELOG](./CHANGELOG.md)

- If you use the `_timeline` "HTML component", it now has an additional sub-class for title. Check the documentation
examples if you want to use it.
- `RuiThumbnails` does not have padding anymore. If you relied on it, use the `-md` helper to compensate.

## From 1.3.0 or 2.0.0-alpha.1 to 2.0.0

Read the [CHANGELOG](./CHANGELOG.md)

If you upgrade from `2.0.0-alpha.1`, there are no breaking change here.

If you upgrade from `1.3.0`, upgrade to `2.0.0-alpha`, then `2.0.0-alpha-1`, then to `2.0.0`.

## From 2.0.0-alpha to 2.0.0-alpha.1

Start by reading the [CHANGELOG](./CHANGELOG.md), then:

- Library exports have changed in `package.json`; you _may_ have to change some import paths in your app:
  - Generated CSS
    ```js
    // From
    import '@experiments-labs/rise_ui/style.css'
    // to:
    import '@experiments-labs/rise_ui/dist/full.css'
    import '@experiments-labs/rise_ui/dist/fonts.css'
    ```
  - Components index: no change in import path. It is still possible to do that:
    ```js
    import { RuiButton, RuiToolbar } from '@experiments-labs/rise_ui/components'
    ```
  - Individual components from source:
    ```js
    // From
    import RuiButton from '@experiments-labs/rise_ui/components/<...>.vue'
    // to:
    import RuiButton from '@experiments-labs/rise_ui/src/components/<...>.vue'
    ```
  - SCSS stylesheets:
    ```scss
    // From
    @use '@experiments-labs/rise_ui/styles/<...>';
    // to:
    @use '@experiments-labs/rise_ui/src/stylesheets/<...>';
    ```
    If you import these files from JS you still have to change the paths.
  - Utility modules:
    ```js
    // From
    import {thing} from '@experiments-labs/rise_ui/utils/<...>'
    // to:
    import {thing} from '@experiments-labs/rise_ui/src/lib/<...>'
    ```
  - Documentation system: no change in import paths
- `RuiToolbar`: If you used the `help` prop, it has been removed. If you used the `help` slot, it has been removed too.
  There is a `right` slot as replacement:
  ```html
  <!-- Replace -->
  <rui-toolbar help="Some help" />
  <!-- by -->
  <rui-toolbar>
    <template #right>
      <rui-help-button content="Some help" />
    </template>
  </rui-toolbar>

  <!-- and -->
  <rui-toolbar>
    <template #help>Complex content</template>
  </rui-toolbar>
  <!-- by -->
  <rui-toolbar>
    <template #right>
      <rui-help-button>Complex content</rui-help-button>
    </template>
  </rui-toolbar>
  ```
  If you have the `floating-vue` dependency installed only to be able to use `RuiToolbar`, you can now remove it.
- CSS: If you imported somehow the whole `style.scss` in your app, rename the import to `full.scss`. If you were only
  importing some of the styles, one of the new flavors may be interesting too as a replacement.
  If you don't want to use the external libraries, use `rise_ui.scss` (and remove the libraries).
- The components relying on external libraries are removed from the components index: `RuiSearchForm`,
  `RuiHelpButton`, `RuiHelpButtonContent`, `RuiRgbaPicker` and `RuiTagSelector`. You will need to import them by yourself:
  ```js
  // Global registration in your main entrypoint (e.g.: main.js):
  // RiseUI extra components
  import RuiHelpButton from '@experiments-labs/rise_ui/src/components/HelpButton/HelpButton.vue'
  import RuiTagSelector from '@experiments-labs/rise_ui/src/components/Inputs/TagSelector/TagSelector.vue'
  import RuiRgbaPicker from '@experiments-labs/rise_ui/src/components/Inputs/RgbaPicker/RgbaPicker.vue'
  import RuiSearchForm from '@experiments-labs/rise_ui/src/components/Form/SearchForm.vue'
  // Register them manually:
  app.component('RuiHelpButton', RuiHelpButton)
  //...
  ```
  You can also import them directly in your components.
- As a side effect of the removal of the extraction of some components from the list, you can remove the following options
  passed during the library initialization: `useDebounce`, `usePopper`, `useVanillaPicker`, `useVueMultiselect`.
- VueJS components are not loaded globally with the `RiseUI` plugin; import them individually instead. If you prefer to continue
  to load them all, register them in your entrypoint.
  ```js
  // All RiseUI components
  import * as ruiComponents from '@experiments-labs/rise_ui/src/components/index.js'

  // Register all the base components
  for (const name in ruiComponents) { app.component(name, ruiComponents[name]) }
  ```
- Documentation system components are not loaded globally with the `DocumentationSystem` plugin; import them individually
  instead. If you prefer to continue to load them all, register them in your documentation entrypoint.
  ```js
  // All RiseUI components
  import * as docComponents from '@experiments-labs/rise_ui/documentation_system/components/index.js'

  // Register all the documentation components
  for (const name in docComponents) { app.component(name, docComponents[name]) }
  ```
- Library is not compiled anymore. Builds are gone. If you don't use ES6 imports, there is no alternative. If you use
  ES6 build, this _should_ be transparent.
- If you used the big generated CSS file and want to continue this way, you will need to change your imports:
  ```js
  // From
  import '@experiments-labs/rise_ui/style.css'
  // to
  import '@experiments-labs/rise_ui/dist/full.css' // or another variant if it better fits.
  import '@experiments-labs/rise_ui/dist/fonts.css' // <-- Add this
  ```
- Fonts are extracted from the stylesheets. If you use the SCSS stylesheets, import it:
  ```scss
  @use '@experiments-labs/rise_ui/styles/rise_ui';
  @use '@experiments-labs/rise_ui/styles/fonts'; // <-- Add this
  ```
  or in your entrypoint:
  ```js
  import '@experiments-labs/rise_ui/src/stylesheets/rise_ui.scss';
  import '@experiments-labs/rise_ui/src/stylesheets/fonts.scss'; // <-- Add this
  ```

## From 1.3.0 to 2.0.0-alpha

Start by reading the [CHANGELOG](./CHANGELOG.md), then:

- Search references to `-text--<color>` and replace them by `-text--color-<color>`.
  You may search and replace
  `-text--(mute|danger|error|destructive|helping|creative|done|success|recording|warning|canceling)`
  by `-text--color-$1`. You also may have some references in string interpolations, so check for them too.
- Search and replace `-flex--center` by `-justify-content-center`. Check for string interpolations too.
- Search and replace `-flex--right` by `-justify-content-right`. Check for string interpolations too.
- Search for usages of `RuiFormErrors` and rename it to `RuiErrors`: replace `<(/?)rui-form-errors` by `<$1rui-errors`.
- On _RuiModal_, replace usages of `has-footer="false"` by `no-footer` and remove `has-footer="true"` if any.
- ToasterJS custom stylesheet has been removed from the project as the library itself was not used.
  If you use it and the RiseUI CSS copy and use the overrides
  [from the previous version](https://gitlab.com/experimentslabs/garden-party/rise-ui/-/blob/f5a26f322560c1bdca911431ea89ed36dee6862c/src/stylesheets/lib/_toast-js.scss).
- If you use the `RandomString` module from RiseUI, its previous deprecated version has been removed:
  ```js
  // Instead of
  import RandomString from '<the path>/RandomString.js'
  // do
  import { RandomString } from '<the path>/String.js'
  ```

## From 1.2.0 to 1.3.0

Update dependencies, especially `debounce` and `floating-vue`

## From 1.1.0 to 1.2.0

`randomString()` from `src/lib/randomString` is deprecated but still available. Use `randomString` from
`src/lib/String.js` if you want to address the change before it's removed.

## From 1.0.0 to 1.1.0

No special action is required

## From 0.1.1 to 1.0.0

No special action is required

## From 0.0.6 to 0.1.0

- `vue3-popper` has been replaced by `floating-vue`. You should add it if you use the components using `vue3-popper`:
  ```sh
  yarn add floating-vue
  ```
  Remove `vue3-popper` if you don't use it, or adapt your components:
  ```sh
   yarn remove vue3-popper
  ```
- We now use Node version 20
- `debounce` has been upgraded from 1.x to 2.0. Update your `package.json` with `"debounce": "^2.0.0",`. Usage slightly
  change, so if you use this library in your own components, change the import statement:
  ```js
  // Before:
  import { debounce } from 'debounce'
  // After:
  import debounce from 'debounce'
  ```
- `@fontsource/ubuntu` has been upgraded from 4.x to 5.0. Upgrade your `package.json` with `"@fontsource/ubuntu": "^5.0.8",`
