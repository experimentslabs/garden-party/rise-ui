# Rise UI

> The frontend framework from [Garden Party](https://garden-party.io)

<div style="font-size: 2em; font-weight: bold; margin: 2em; text-align: center">
  This project is a work in progress. Come back later...
</div>

## Preamble

Garden Party is a [Ruby on Rails](https://rubyonrails.org) application. It's administrative section is plain old Rails
views, and the _app_ part is written with [VueJS](https://vuejs.org/) 3.

The CSS used in the project had to be both available to VueJS components as to administrative views, so:

- you can use only the CSS, without VueJS
- you _should_ be able to pick only the CSS components you need by using the [SCSS](https://sass-lang.com/) files
- styles are not scoped in VueJS components
- if you don't use VueJS, components won't be dynamic/reactive, you will need to do things yourselves (an integration
  with [stimulus](https://stimulus.hotwired.dev/) is welcome).

## Links

- Code: https://gitlab.com/experimentslabs/garden-party/rise-ui/
- Issues: https://gitlab.com/experimentslabs/garden-party/rise-ui/-/issues
- Development chat (Matrix): [#rise-ui-dev:matrix.org](https://matrix.to/#/#rise-ui-dev:matrix.org)

## Usage

Add the dependency to your project:

```sh
yarn add @experiments-labs/rise_ui
```

Read the [Using styles](#using-styles) and [Integration with VueJS](#integration-with-vuejs) to continue.

## Using styles

Styles are exposed in `/styles/*` in the package, and matches `src/stylesheets` in the repository.

```text
src/stylesheets
├── style.scss             # Entrypoint: Use everything
│
├── components             # HTML components without a VueJS equivalent, prefixed with "_". E.g.: _card
├── _components.scss       # Convenient index
│
├── core                   # Anything but selectors: variables, mixins, common placeholders...
├── _core.scss             # Convenient index
│
├── helpers                # Helper classes, prefixed with "-". E.g.: -mtd
├── _helpers.scss          # Convenient index
│
├── lib                    # Custom library overrides
├── _lib.scss              # Convenient index
│
├── reset                  # HTML tags
├── _reset.scss            # Convenient index
│
├── themes                 # Themes: CSS variable definitions
├── _themes.scss           # Defines light and dark theme.
│
├── vue_components         # HTML components without a VueJS equivalent, prefixed with "_". E.g.: _card
└── _vue_components.scss   # Convenient index
```

Styles for specific VueJS components are exposed in `/components/*` in the package, and matches `src/components/*` in
the repository.

### Usage

To use everything:

```js
// some_entrypoint.js

import '@experiments-labs/rise_ui/styles.css'
```

To import only the styles you need:

```scss
// your_stylesheet.scss

@use '@experiments-labs/rise_ui/styles/reset';
@use '@experiments-labs/rise_ui/styles/themes';
@use '@experiments-labs/rise_ui/styles/...';
@use '@experiments-labs/rise_ui/components/Button/Button';
@use '@experiments-labs/rise_ui/components/...';
```

### Naming convention

To keep this project consistent, we follow this convention when it comes to naming:

- `-*` is for helpers (e.g.: `-mtd` for "margin top default")
- `_*` is for components without a VueJS counterpart (e.g.: `_card`)
- `rui-*` is for VueJS components (e.g.: `rui-button`)

We try to follow the [BEM](https://getbem.com/) convention for components parts and variants.

## Integration with VueJS

In your entry point (sometimes `main.js`):

```js
//...
import RiseUI from 'rise_ui'
import 'rise_ui/dist/style.css'

const app = createVue(App)
        .use(RiseUI)
//...
```

### Options

You can pass an object of options to `app.use()`:

```js
app.use(RiseUI, {
   // These are the defaults; you should set them to "false" if you don't don't want
   // to add the dependencies.
   // Check "src/library.js" for the "componentsLibraries" constant to have a list
   // of these components.
   useDebounce: true,       // Whether to enable components relying on "debounce" library
   usePopper: true,         // Whether to enable components relying on "vue3-popper" library
   useVanillaPicker: true,  // Whether to enable components relying on "vanilla-picker" library
   useVueMultiselect: true, // Whether to enable components relying on "vue-multiselect" library
   // Check "I18n" below for these options
   useVueI18n: false,
   locale: 'en',
   locales: {},
   // Check "Object attributes in form errors" below
   i18nObjectAttributesPath: null,
   // Check "Icons" below
   iconsFile: null
})
```

To enable components relying on other dependency, you will have to manually add these to your project.

### Import components individually

Components can be imported from `@experiments-labs/rise_ui/components/*` and from the index `@experiments-labs/rise_ui/components`:

```js
import RuiButton from '@experiments-labs/rise_ui/components/Button/Button.vue'
import RuiIcon from '@experiments-labs/rise_ui/components/Icon/Icon.vue'
// Or
import { RuiButton, RuiIcon } from '@experiments-labs/rise_ui/components'
```

### I18n

**Note**: Check documentation for usage and override examples.

RiseUI was meant to be used with `vue-i18n`. If you don't want to handle internationalization in your application, a fallback `$t` method is injected globally in Vue components, so components that use internationalization still render.

The available translations are _en_ and _fr_ (in `src/locales`) for now, feel free to contribute with more.

Rise UI options are:

- `useVueI18n`: whether to use `vue-i18n` or provide and use a fake `$t` method.
- `locale`: default locale to use when not using `vue-i18n`
- `locales`: Custom object of translations to use when not using `vue-i18n`

### Object attributes in form errors

`RuiFormError` component was meant to be used with a Rails API where for errors usually looks like:

```json
{
   "name": [
      "is blank"
   ]
}
```

While Rails translates the messages, the fields names are provided as-is and should be translated to the user.

`i18nObjectAttributesPath` is a string representing the path in the translations object where the model attributes translations can be found.

In a Rails project, it's usually `activerecord.attributes`.

To use it with custom attributes, create the translations in the `locales` option like:

```js
const locales = {
  en: {
    object_attributes: {
      my_model: {
        name: 'Name',
        some_field: 'Some field'
      }
    }
  }
}
```

Refer to the `RuiFormErrors` component for usage.

### Icons

RiseUI comes with a few icons, originally used on Garden Party. If you want to use your own,
you should provide an SVG icon set, with valid icons for RiseUI plus yours.

**Note**: This section is incomplete and really needs improvement.

## Contribution

Contributions are welcome! To ensure your changes won't be rejected, you _should open an issue first_ to describe what
you plan to do.

1. Clone the repository
   ```sh
   git clone git@gitlab.com:experimentslabs/garden-party/rise-ui.git
   # or
   git clone https://gitlab.com/experimentslabs/garden-party/rise-ui.git
   ```
2. Install dependencies
   ```sh
   yarn install
   ```
3. Start documentation project to preview changes
   ```sh
   yarn dev
   ```
4. Lint the files
   ```sh
   yarn lint:js    # use --fix to autofix issues
   yarn lint:style # use --fix to autofix issues
   ```
5. Run the tests
   ```sh
   yarn test
   ```
6. Update Readme if needed. Update Changelog accordingly. Commit your changes (we adhere to the
   [Conventional Commits](https://www.conventionalcommits.org/) convention in this project)
7. Create a merge request; wait for reviews and merge
8. Celebrate!

### Development scripts

From `package.json`'s `scripts` section:

```sh
# Start documentation project to preview components as you develop them
yarn dev
# Builds the library
yarn build
# Builds the documentation
yarn build:docs
# Lint javascript an vue files. Use --fix to auto-fix issues
yarn lint:js
# Lint SCSS and css files. Use --fix to auto-fix issues
yarn lint:style
# Runs the tests
yarn test
```

### Links

#### Possible IDE Setup

Any text editor with support for syntax coloration will work, however we feel more comfortable to use a tool allowing us
to quickly jump from file to file, access documentation and other things that "takes time"...

- [VSCodium](https://vscodium.com//) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur)
- WebStorm with plugins for VueJS and SCSS
- ...

Note: If you successfully use Vim/Emacs with plugins, what is your setup?

## Changes

All the changes are documented in the [CHANGELOG](CHANGELOG.md).
