# Rise UI

> The frontend framework from [Garden Party](https://garden-party.io)

## Preamble

Garden Party is a [Ruby on Rails](https://rubyonrails.org) application. It's administrative section is plain old Rails
views, and the _app_ part is written with [VueJS](https://vuejs.org/) 3.

The CSS used in the project had to be both available to VueJS components as to administrative views, so:

- you can use only the CSS, without VueJS
- you _should_ be able to pick only the CSS components you need by using the [SCSS](https://sass-lang.com/) files
- styles are not scoped in VueJS components
- if you don't use VueJS, components won't be dynamic/reactive, you will need to do things yourselves (an integration
  with [stimulus](https://stimulus.hotwired.dev/) is welcome).

## Links

- Code: https://gitlab.com/experimentslabs/garden-party/rise-ui/
- Issues: https://gitlab.com/experimentslabs/garden-party/rise-ui/-/issues
- Development chat (Matrix): [#rise-ui-dev:matrix.org](https://matrix.to/#/#rise-ui-dev:matrix.org)
- [Code of conduct](CODE_OF_CONDUCT.md)
- [Contribution guide](CONTRIBUTING.md)
- [Migration guide](MIGRATION_GUIDE.md)
- [Change log](CHANGELOG.md)

## Usage

Add the dependency to your project:

```sh
yarn add @experiments-labs/rise_ui
```

### Usage: CSS/SCSS only

This section targets HTML-only apps. If your application is made of HTML pages and VueJS components, check the next
section.

If you don't care about the CSS size and want it all to be available quickly, use the generated CSS files:

- `dist/fonts.css`: (~1.2M) @fontface definition with embedded fonts
plus one of:
- `dist/minimal.css`: (~43K) style reset and CSS helpers only
- `dist/with_css_components`: (~48K) minimal + the few "HTML-only" component styles (not starting with "Rui" in the docs)
- `dist/rise_ui.css`: (~87K) all of the above, plus the styles for the VueJS components (starting with "Rui) in the docs)
- `dist/full.css`: (~100K) all of the above, plus styles for a few JS libraries (may not be useful for HTML-only applications)

If you want to use the SCSS files:

```scss
@use '@experiments-labs/rise_ui/src/stylesheets/fonts'; // @fontfaces, recommended unless you want to use your own
// And one of
@use '@experiments-labs/rise_ui/src/stylesheets/minimal';
@use '@experiments-labs/rise_ui/src/stylesheets/with_css_components';
@use '@experiments-labs/rise_ui/src/stylesheets/rise_ui';
@use '@experiments-labs/rise_ui/src/stylesheets/full';
```
You can also make your own selection to have more control on what's included (check the files above to see what you can pick)

Then, add the `.rui` class to the `<body>` of your pages. It will use the light or dark theme depending on the browser settings.
To force the dark or light theme, add `.rui--theme-light` or `.rui--theme-dark` to the `body` tag.

For more details on how to customize the colors, spaces, etc... check the CSS variables section of the documentation.

### Usage: VueJS

```js
// Main entrypoint (generally main.js)

import { createApp } from 'vue'
import { RiseUI } from '@experiments-labs/rise_ui'

import App from './App.vue'

const app = createApp(App)
  .use(RiseUI, {})

app.mount('#app')
```

That's the base; you won't have components with this setup. You can register all of them at once in the app creation
process, register only the ones you need, or import them when needed in your components/views:

Import all (will increase the bundle size for nothing if you don't use them all):

```js
// main.js
import { createApp } from 'vue'

import { RiseUI } from '@experiments-labs/rise_ui'
import * as ruiComponents from '@experiments-labs/rise_ui/components'
// Appropriate stylesheet:
import '@experiments-labs/rise_ui/src/stylesheets/rise_ui.scss'

const app = createApp(App)
  .use(RiseUI, {})

// Register all the components
for(const name in ruiComponents) { app.component(name, ruiComponents[name]) }

app.mount('#app')
```

Import what you need:

```js
// main.js

import { RuiButton, RuiToolbar } from '@experiments-labs/rise_ui/components'
// Appropriate stylesheets:
import '@experiments-labs/rise_ui/src/stylesheets/with_css_components.scss'
import './rise_ui_components.scss' // File where you will only import the styles you need

const app = createApp(App)
  .use(RiseUI, {})
  .component('RuiButton', RuiButton)
  .component('RuiToolbar', RuiToolbar)

app.mount('#app')
```

Import in components:

- Import the stylesheets in the app entrypoint.
- Import the used components in yours.

**Note**: some components are based on external dependencies and have to be manually imported/registered:

- `RuiSearchForm`: relies on `debounce`
- `RuiHelpButton`, `RuiHelpButtonContent`: relies on `floating-vue`
- `RuiRgbaPicker`: relies on `vanilla-picker`
- `RuiTagSelector`: relies on `vue-multiselect`

Their stylesheets are only included in the _full_ style variant but can be imported one by one.

### Usage: mixed app

Import the CSS/SCSS files in a global stylesheet and not in the VueJS app so they are available anywhere.

## Styles organisation

```text
src/stylesheets
├── fonts.scss             # @fontface definitions of the used fonts. Required unless you want to use something different.
│
├── full.scss              # Prepared variants
├── minimal.scss
├── rise_ui.scss
├── with_css_components.scss
│
├── components             # HTML components without a VueJS equivalent, prefixed with "_". E.g.: _card
├── _components.scss       # Convenient index
│
├── core                   # Anything but selectors: SCSS variables, mixins, common placeholders...
├── _core.scss             # Convenient index
│
├── helpers                # Helper classes, prefixed with "-". E.g.: -mtd
├── _helpers.scss          # Convenient index
│
├── lib                    # External library overrides
├── _lib.scss              # Convenient index
│
├── reset                  # HTML reset (tags only)
├── _reset.scss            # Convenient index
│
├── themes                 # Themes: CSS variable definitions
├── _themes.scss           # Defines light and dark theme.
│
└── _vue_components.scss   # Convenient index for all Vue components (starting with "rui-". E.g.: "rui-button")
```

Styles for specific VueJS components are located "near" the component it styles, in `src/components/`

That's up to you to use an available variant with the risk of having unneeded classes in the final bundle,
or compose your own stylesheet.

Keep in mind that the VueJS component are also based on helpers, so you may want to include them.

### Naming convention

To keep this project consistent, we follow this convention when it comes to naming:

- `-*` is for helpers (e.g.: `-mtd` for "margin top default")
- `_*` is for components without a VueJS counterpart (e.g.: `_card`)
- `rui-*` is for VueJS components (e.g.: `rui-button`)

We try to follow the [BEM](https://getbem.com/) convention for components parts and variants.

## VueJS configuration options

### Options

You can pass an object of options to `app.use()`:

```js
app.use(RiseUI, {
  // These are the defaults
  useVueI18n: false,
  locale: 'en',
  locales: {},
  // Check "Object attributes in form errors" below
  i18nObjectAttributesPath: null,
  // Default icon pack.
  // See the icon section of the documentation to see how it works
  iconsFile: null,
  // Other icons packs
  iconsPacks: {}
})
```

### I18n

**Note**: Check documentation for usage and override examples.

RiseUI was meant to be used with `vue-i18n`. If you don't want to handle internationalization in your application, a
fallback `$t` method is injected globally in Vue components, so components that use internationalization still render.

The available translations are _en_ and _fr_ (in `src/locales`) for now, feel free to contribute with more.

Rise UI options are:

- `useVueI18n`: whether to use `vue-i18n` or provide and use a fake `$t` method.
- `locale`: default locale to use when not using `vue-i18n`
- `locales`: Custom object of translations to use when not using `vue-i18n`

### Object attributes in form errors

`RuiError` component was meant to be used with a Rails API where form errors usually looks like:

```json
{
   "name": [
      "is blank"
   ]
}
```

While Rails translates the messages, the fields names are provided as-is and should be translated to the user.

`i18nObjectAttributesPath` is a string representing the path in the translations object where the model attributes translations can be found.

In a Rails project, it's usually `activerecord.attributes`.

To use it with custom attributes, create the translations in the `locales` option like:

```js
const locales = {
  en: {
    object_attributes: {
      my_model: {
        name: 'Name',
        some_field: 'Some field'
      }
    }
  }
}
```

Refer to the `RuiErrors` component for usage.

### Icons

RiseUI comes with a few base icons from [Phosphor Icons](https://phosphoricons.com/) (see [./LICENSES.md]), usable with prefix `rui:`. You can add your own and override the defaults during
plugin configuration; refer to the [Icons documentation](./docs/src/markdown/icons-setup.md) for the detailed setup.

## Development notes

### Scripts

From `package.json`'s `scripts` section:

```sh
# Start documentation project to preview components as you develop them
yarn dev
# Builds the CSS in `dist/`
yarn build:styles
# Builds the documentation
yarn build:docs
# Generates documentation metadata. The generated files are not versioned
yarn docs:metadata
# Lint javascript an vue files. Use --fix to auto-fix issues
yarn lint:js
# Lint SCSS and css files. Use --fix to auto-fix issues
yarn lint:style
# Runs the tests in watch mode
yarn test
# Runs the tests once and exits
yarn release:test
```

### Links

#### Possible IDE Setup

Any text editor with support for syntax coloration will work, however we feel more comfortable to use a tool allowing us
to quickly jump from file to file, access documentation and other things that "takes time"...

- [VSCodium](https://vscodium.com//) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur)
- WebStorm with plugins for VueJS and SCSS
- ...

Note: If you successfully use Vim/Emacs with plugins, what is your setup?
