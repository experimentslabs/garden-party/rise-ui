// Using plain paths because it's easy to use them when project don't use vue-i18n
export default {
  rui: {
    confirm_dialog: { are_you_sure: 'Êtes vous sûr (e) ? ' },
    form_errors: { has_errors: 'Veuillez vérifier le formulaire sur les points suivants :' },
    generic: {
      cancel: 'Annuler',
      close: 'Fermer',
      definition: '{term} :',
      ok: 'OK',
      save: 'Enregistrer',
    },
    help_button: { help: 'Aide' },
    image_viewer: {
      a_picture: 'Une image',
      a_thumbnail: 'Une miniature',
      next: 'Suivant',
      previous: 'Précédent',
    },
    input: {
      clear: 'Effacer',
    },
    search: {
      placeholder: 'Saisissez quelque chose à chercher...',
      search: 'Rechercher',
    },
  },
}
