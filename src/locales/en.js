// Using plain paths because it's easy to use them when project don't use vue-i18n
export default {
  rui: {
    confirm_dialog: { are_you_sure: 'Are you sure?' },
    form_errors: { has_errors: 'Please, check the form on the following points:' },
    generic: {
      cancel: 'Cancel',
      close: 'Close',
      definition: '{term}:',
      ok: 'OK',
      save: 'Save',
    },
    help_button: { help: 'Help' },
    image_viewer: {
      a_picture: 'A picture',
      a_thumbnail: 'A thumbnail',
      next: 'Next',
      previous: 'Previous',
    },
    search: { search: 'Search' },
  },
}
