import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Icon from './Icon.vue'

function makeWrapper (name) {
  return mount(Icon, {
    props: {
      name,
    },
  })
}

let wrapper

describe('Icon', () => {
  beforeEach(() => {
    wrapper = makeWrapper('user')
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
