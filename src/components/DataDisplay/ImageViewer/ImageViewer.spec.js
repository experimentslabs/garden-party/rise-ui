import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import ImageViewer from './ImageViewer.vue'

function makeWrapper (preselected) {
  return mount(ImageViewer, {
    props: {
      preselected,
    },
  })
}

let wrapper

describe('ImageViewer', () => {
  beforeEach(() => {
    wrapper = makeWrapper('url-to-some-picture')
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
