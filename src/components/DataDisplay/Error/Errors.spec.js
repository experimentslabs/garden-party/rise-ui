import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Errors from './Errors.vue'

function makeWrapper () {
  return mount(Errors)
}

let wrapper

describe('Errors', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
