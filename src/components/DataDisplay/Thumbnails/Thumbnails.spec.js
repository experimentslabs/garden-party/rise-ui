import { describe, beforeEach, beforeAll, afterAll, it, expect, vi } from 'vitest'
import { mount } from '@vue/test-utils'

import Thumbnails from './Thumbnails.vue'

function makeWrapper (list) {
  return mount(Thumbnails, {
    props: {
      list,
    },
  })
}

let wrapper

describe('Thumbnails', () => {
  beforeAll(() => {
    vi.stubGlobal('IntersectionObserver', vi.fn(() => ({
      disconnect: vi.fn(),
      observe: vi.fn(),
      takeRecords: vi.fn(),
      unobserve: vi.fn(),
    })))
  })

  afterAll(() => {
    vi.unstubAllGlobals()
  })

  beforeEach(() => {
    wrapper = makeWrapper([])
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
