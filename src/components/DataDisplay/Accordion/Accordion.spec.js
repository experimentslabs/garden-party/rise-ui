import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Accordion from './Accordion.vue'

function makeWrapper () {
  return mount(Accordion)
}

let wrapper

describe('Accordion', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
