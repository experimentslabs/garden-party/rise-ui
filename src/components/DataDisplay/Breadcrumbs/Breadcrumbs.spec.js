import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Breadcrumbs from './Breadcrumbs.vue'

function makeWrapper (chunks) {
  return mount(Breadcrumbs, {
    props: {
      chunks,
    },
    global: {
      stubs: ['router-link'],
    },
  })
}

let wrapper

describe('Breadcrumbs', () => {
  beforeEach(() => {
    wrapper = makeWrapper(['some', 'thing'])
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
