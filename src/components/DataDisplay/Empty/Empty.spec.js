import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Empty from './Empty.vue'

function makeWrapper () {
  return mount(Empty)
}

let wrapper

describe('Empty', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
