import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Tag from './Tag.vue'

function makeWrapper (tag) {
  return mount(Tag, {
    props: {
      tag,
    },
  })
}

let wrapper

describe('Tag', () => {
  beforeEach(() => {
    wrapper = makeWrapper('the tag')
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
