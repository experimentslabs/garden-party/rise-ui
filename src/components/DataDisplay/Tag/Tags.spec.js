import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Tags from './Tags.vue'

function makeWrapper (tags) {
  return mount(Tags, {
    props: {
      tags,
    },
  })
}

let wrapper

describe('Tags', () => {
  beforeEach(() => {
    wrapper = makeWrapper(['a tag', 'another'])
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
