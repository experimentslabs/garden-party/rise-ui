import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import SubmitButton from './SubmitButton.vue'

function makeWrapper () {
  return mount(SubmitButton)
}

let wrapper

describe('SubmitButton', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
