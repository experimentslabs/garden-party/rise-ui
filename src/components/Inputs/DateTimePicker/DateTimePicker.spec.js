import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import DateTimePicker from './DateTimePicker.vue'

function makeWrapper (inputName) {
  return mount(DateTimePicker, {
    props: {
      inputName,
    },
  })
}

let wrapper

describe('DateTimePicker', () => {
  beforeEach(() => {
    wrapper = makeWrapper('the_input')
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
