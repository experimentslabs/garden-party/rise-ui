import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import TimeOutButton from './TimeOutButton.vue'

function makeWrapper () {
  return mount(TimeOutButton)
}

let wrapper

describe('TimeOutButton', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
