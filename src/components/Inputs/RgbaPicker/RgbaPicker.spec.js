import { beforeEach, describe, expect, it, vi } from 'vitest'
import { mount } from '@vue/test-utils'
import RgbaPicker from './RgbaPicker.vue'

/**
 * @typedef {import('vue')}                          Vue
 * @typedef {import('@vue/test-utils').VueWrapper  } Wrapper
 */

let wrapper

/**
 * @returns {Wrapper<Vue>} Mounted wrapper for the test
 */
function makeParentWrapper () {
  return mount({
    template: '<template><rgba-picker v-model="input" ref="picker" /></template>',
    components: { RgbaPicker },
    data () {
      return { input: '0,0,0,0' }
    },
  })
}

describe('bindings with parent', () => {
  beforeEach(() => {
    wrapper = makeParentWrapper()
  })

  it('mounts', () => {
    expect(wrapper.element).toMatchSnapshot()
  })

  describe('interactions', () => {
    describe('when changing a value', () => {
      beforeEach(async () => {
        // Call the callback to simulate a change
        await wrapper.vm.$refs.picker.picker.onChange({ rgba: [255, 0, 0, 0] })
      })

      it('updates parents state', () => {
        expect(wrapper.vm.input).toBe('255,0,0,0')
      })
    })

    describe('when changing parent state', () => {
      let onChangeMock

      beforeEach(async () => {
        onChangeMock = vi.fn()
        wrapper.vm.$refs.picker.picker.setColor = onChangeMock
        await wrapper.setData({ input: '1,1,1,1' })
      })

      it('updates the Inputs data', async () => {
        expect(onChangeMock).toHaveBeenCalledWith([1, 1, 1, 1], true)
      })
    })
  })
})
