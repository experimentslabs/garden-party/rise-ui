import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Drawer from './Drawer.vue'

function makeWrapper () {
  return mount(Drawer)
}

let wrapper

describe('Drawer', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
