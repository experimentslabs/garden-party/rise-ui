import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Badge from './Badge.vue'

function makeWrapper () {
  return mount(Badge)
}

let wrapper

describe('Badge', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
