import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Toolbars from './Toolbars.vue'

function makeWrapper () {
  return mount(Toolbars)
}

let wrapper

describe('Toolbars', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
