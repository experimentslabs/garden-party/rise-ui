import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Toolbar from './Toolbar.vue'

function makeWrapper () {
  return mount(Toolbar)
}

let wrapper

describe('Toolbar', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
