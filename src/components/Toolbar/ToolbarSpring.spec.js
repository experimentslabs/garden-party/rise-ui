import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import ToolbarSpring from './ToolbarSpring.vue'

function makeWrapper () {
  return mount(ToolbarSpring)
}

let wrapper

describe('ToolbarSpring', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
