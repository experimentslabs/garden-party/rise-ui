import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import ToolbarSeparator from './ToolbarSeparator.vue'

function makeWrapper () {
  return mount(ToolbarSeparator)
}

let wrapper

describe('ToolbarSeparator', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
