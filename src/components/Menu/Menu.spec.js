import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Menu from './Menu.vue'

function makeWrapper () {
  return mount(Menu)
}

let wrapper

describe('Menu', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
