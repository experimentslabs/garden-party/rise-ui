import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Dropdown from './Dropdown.vue'

function makeWrapper () {
  return mount(Dropdown)
}

let wrapper

describe('Dropdown', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
