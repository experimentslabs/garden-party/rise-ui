import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Loading from './Loading.vue'

function makeWrapper () {
  return mount(Loading)
}

let wrapper

describe('Loading', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
