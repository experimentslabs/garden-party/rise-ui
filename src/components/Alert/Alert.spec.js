import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Alert from './Alert.vue'

function makeWrapper () {
  return mount(Alert)
}

let wrapper

describe('Alert', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
