import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Actions from './Actions.vue'

function makeWrapper () {
  return mount(Actions)
}

let wrapper

describe('Actions', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
