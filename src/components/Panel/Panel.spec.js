import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Panel from './Panel.vue'

function makeWrapper () {
  return mount(Panel)
}

let wrapper

describe('Panel', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
