import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Item from './Item.vue'

function makeWrapper (caption) {
  return mount(Item, {
    props: {
      caption,
    },
  })
}

let wrapper

describe('Item', () => {
  beforeEach(() => {
    wrapper = makeWrapper('An item')
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
