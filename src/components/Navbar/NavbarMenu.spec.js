import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import NavbarMenu from './NavbarMenu.vue'

function makeWrapper () {
  return mount(NavbarMenu)
}

let wrapper

describe('NavbarMenu', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
