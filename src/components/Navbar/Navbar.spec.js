import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Navbar from './Navbar.vue'

function makeWrapper (caption) {
  return mount(Navbar, {
    props: {
      caption,
    },
  })
}

let wrapper

describe('Navbar', () => {
  beforeEach(() => {
    wrapper = makeWrapper('The caption')
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
