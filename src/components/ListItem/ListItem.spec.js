import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import ListItem from './ListItem.vue'

function makeWrapper () {
  return mount(ListItem)
}

let wrapper

describe('ListItem', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
