import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import CollapsibleListItem from './CollapsibleListItem.vue'

function makeWrapper () {
  return mount(CollapsibleListItem)
}

let wrapper

describe('CollapsibleListItem', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
