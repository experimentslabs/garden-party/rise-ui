import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import HelpButtonContent from './HelpButtonContent.vue'

function makeWrapper () {
  return mount(HelpButtonContent)
}

let wrapper

describe('HelpButtonContent', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
