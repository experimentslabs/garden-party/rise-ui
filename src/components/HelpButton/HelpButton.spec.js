import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import HelpButton from './HelpButton.vue'

function makeWrapper () {
  return mount(HelpButton, {
    global: {
      stubs: {
        popper: {
          template: '<div class="rui-help-button"><slot/></div>',
        },
      },
    },
  })
}

let wrapper

describe('HelpButton', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
