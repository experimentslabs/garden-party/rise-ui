import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Overlay from './Overlay.vue'

function makeWrapper () {
  return mount(Overlay)
}

let wrapper

describe('Overlay', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
