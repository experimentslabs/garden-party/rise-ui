import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Form from './Form.vue'

function makeWrapper () {
  return mount(Form)
}

let wrapper

describe('Form', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
