import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Field from './Field.vue'

function makeWrapper () {
  return mount(Field)
}

let wrapper

describe('Field', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
