import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import FormErrors from './FormErrors.vue'

function makeWrapper () {
  return mount(FormErrors)
}

let wrapper

describe('FormErrors', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
