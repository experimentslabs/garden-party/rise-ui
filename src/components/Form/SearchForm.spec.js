import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import SearchForm from './SearchForm.vue'

function makeWrapper (modelValue = '') {
  return mount(SearchForm, {
    props: {
      modelValue,
    },
  })
}

let wrapper

describe('SearchForm', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
