import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import IconText from './IconText.vue'

function makeWrapper (icon, text) {
  return mount(IconText, {
    props: {
      icon,
      text,
    },
    global: {
      mocks: {
        $_ruiIconsFiles: { default: '' },
      },
    },
  })
}

let wrapper

describe('IconText', () => {
  beforeEach(() => {
    wrapper = makeWrapper('user', 'Sherlock')
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
