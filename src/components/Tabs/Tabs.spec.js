import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Tabs from './Tabs.vue'

function makeWrapper () {
  return mount(Tabs)
}

let wrapper

describe('Tabs', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
