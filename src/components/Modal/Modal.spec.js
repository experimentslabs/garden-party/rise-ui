import { beforeEach, describe, expect, it } from 'vitest'
import { mount } from '@vue/test-utils'
import ModalComponent from './Modal.vue'

/**
 * @typedef {import('vue')}                          Vue
 * @typedef {import('@vue/test-utils').VueWrapper  } Wrapper
 */

/**
 * @param   {object}       props - Custom props
 * @param   {object}       slots - Slot values
 * @returns {Wrapper<Vue>}       Mounted wrapper for the test
 */
function makeWrapper (props = {}, slots = {}) {
  return mount(ModalComponent, {
    props,
    slots,
    global: {
      stubs: ['RuiIcon'],
    },
  })
}

let wrapper

describe('interface', () => {
  describe('with only title prop', () => {
    beforeEach(() => {
      wrapper = makeWrapper({ caption: 'Hello world' })
    })

    it('displays title', () => {
      expect(wrapper.text()).toContain('Hello world')
    })

    it('has only the validation button by default', () => {
      expect(wrapper.text()).toContain('generic.ok')
      expect(wrapper.findAll('.rui-modal__footer button')).toHaveLength(1)
    })
  })

  describe('with cancellable option', () => {
    beforeEach(() => {
      wrapper = makeWrapper({
        caption: 'Hello world',
        cancellable: true,
      })
    })

    it('has a cancel button', () => {
      expect(wrapper.text()).toContain('generic.cancel')
    })
  })

  describe('without footer', () => {
    beforeEach(() => {
      wrapper = makeWrapper({
        caption: 'Hello world',
        noFooter: true,
      })
    })

    it('has no footer', () => {
      expect(wrapper.text()).not.toContain('generic.cancel')
      expect(wrapper.text()).not.toContain('generic.ok')
    })
  })

  describe('with a slot', () => {
    let slotContent

    beforeEach(() => {
      slotContent = 'This is the slot content'
      wrapper = makeWrapper({
        caption: 'Hello world',
        noFooter: true,
      },
      { default: slotContent })
    })

    it('renders slot', () => {
      expect(wrapper.text()).toContain(slotContent)
    })
  })
})

// TODO: Test interactions
