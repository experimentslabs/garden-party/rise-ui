import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import ConfirmDialog from './ConfirmDialog.vue'

function makeWrapper () {
  return mount(ConfirmDialog)
}

let wrapper

describe('ConfirmDialog', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
