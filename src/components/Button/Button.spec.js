import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Button from './Button.vue'

function makeWrapper (options) {
  return mount(Button, options)
}

let wrapper

describe('Button', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })

  describe('with "href" prop', () => {
    beforeEach(() => {
      wrapper = makeWrapper({ props: { href: 'https://garden-party.io/' } })
    })

    it('creates a link', () => {
      expect(wrapper.element.href).toEqual('https://garden-party.io/')
    })

    describe('when disabled', () => {
      beforeEach(() => {
        wrapper = makeWrapper({ props: { href: 'https://garden-party.io/', disabled: true } })
      })
      it('creates a link without "href"', () => {
        expect(wrapper.element.tagName).toBe('A')
        expect(wrapper.element.href).toBe('')
      })
    })

    describe('with "to" prop', () => {
      beforeEach(() => {
        wrapper = makeWrapper({ props: { href: 'https://garden-party.io/', to: '/' } })
      })

      it('creates a router link and ignores "href"', () => {
        // There is no router configured, so the link is broken
        expect(wrapper.element.href).toEqual(undefined)
      })
    })
  })

  describe('with "to" prop', () => {
    beforeEach(() => {
      wrapper = makeWrapper({ props: { to: '/' } })
    })

    it('creates a router link', () => {
      expect(wrapper.element.tagName).toBe('ROUTERLINK')
      expect(wrapper.element.getAttribute('to')).toBe('/')
    })

    describe('when disabled', () => {
      beforeEach(() => {
        wrapper = makeWrapper({ props: { to: '/', disabled: true } })
      })
      it('creates a link without a "href"', () => {
        expect(wrapper.element.tagName).toBe('A')
        expect(wrapper.element.href).toBe('')
      })
    })
  })

  describe('without "to" nor "href" prop', () => {
    beforeEach(() => {
      wrapper = makeWrapper({ props: { caption: 'Hello world' } })
    })

    it('creates a "button"', () => {
      expect(wrapper.element.tagName).toBe('BUTTON')
      expect(wrapper.element.textContent).toBe('Hello world')
    })
  })
})
