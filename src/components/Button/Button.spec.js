import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import Button from './Button.vue'

function makeWrapper () {
  return mount(Button)
}

let wrapper

describe('Button', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect(wrapper.exists()).toBe(true)
  })
})
