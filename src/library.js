import './stylesheets/style.scss'

import * as components from './components'
import locales from './locales'
import icons from './assets/images/icons.svg'

/** @typedef {import('vue').App} VueApp */

// Components depending on specific libraries
const componentsLibraries = {
  debounce: ['RuiSearchForm'],
  'vue3-popper': ['RuiHelpButton'],
  'vue-multiselect': ['RuiTagSelector'],
  'vanilla-picker': ['RuiRgbaPicker'],
}

export const RiseUI = {
  /**
   * @param {VueApp}      app                              - Vue Application
   * @param {object}      options                          - Library options
   * @param {boolean}     options.useDebounce              - Whether to enable components relying on "debounce" library
   * @param {boolean}     options.usePopper                - Whether to enable components relying on "vue3-popper" library
   * @param {boolean}     options.useVanillaPicker         - Whether to enable components relying on "vanilla-picker" library
   * @param {boolean}     options.useVueMultiselect        - Whether to enable components relying on "vue-multiselect" library
   * @param {boolean}     options.useVueI18n               - Whether to use the "vue-i18n" library or inject dummy methods
   * @param {string}      options.locale                   - Locale to use when `useI18n` is `false`
   * @param {object}      options.locales                  - Object of locales overrides
   * @param {string|null} options.i18nObjectAttributesPath - I18n path to translated attributes names. Check Readme for configuration
   * @param {string|null} options.iconsFile                - Custom path to icon file
   */
  install (app, {
    useDebounce = true,
    usePopper = true,
    useVanillaPicker = true,
    useVueMultiselect = true,
    useVueI18n = false,
    locale = 'en',
    locales = {},
    i18nObjectAttributesPath = null,
    iconsFile = null,
  }) {
    // Merge default translations with "locales" values
    if (Object.keys(locales).length > 0) Object.assign(translations, locales)

    // Attributes translations key in locales
    app.config.globalProperties.$_ruiI18nObjectAttributesPath = i18nObjectAttributesPath

    // Setup fallback $t method
    if (!useVueI18n) app.config.globalProperties.$t = (path) => translations[locale][path] || path

    // Icons file
    app.config.globalProperties.$_ruiIconsFile = iconsFile || icons

    // Add components
    for (const name in components) {
      // Ignore components depending on options
      if (!useDebounce && componentsLibraries.debounce.includes(name)) continue
      if (!usePopper && componentsLibraries['vue3-popper'].includes(name)) continue
      if (!useVanillaPicker && componentsLibraries['vanilla-picker'].includes(name)) continue
      if (!useVueMultiselect && componentsLibraries['vue-multiselect'].includes(name)) continue

      app.component(name, components[name])
    }
  },
}

export const translations = locales
