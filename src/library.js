import { makeFakeT } from './lib/FakeT.js'
import locales from './locales'
import libIcons from './assets/images/icons.svg'

/** @typedef {import('vue').App} VueApp */

/**
 * @typedef RiseUiOptions
 * @property {boolean}     useVueI18n               - Whether to use the "vue-i18n" library or inject dummy methods
 * @property {string}      locale                   - Locale to use when `useI18n` is `false`
 * @property {object}      locales                  - Object of locales overrides
 * @property {string|null} i18nObjectAttributesPath - I18n path to translated attributes names. Check Readme for configuration
 * @property {string|null} iconsFile                - Custom path to default icons file
 * @property {object}      iconsPacks               - Additional icon files
 */

export const RiseUI = {
  /**
   * @param {VueApp}        app     - Vue Application
   * @param {RiseUiOptions} options - Library options
   */
  install (app, {
    useVueI18n = false,
    locale = 'en',
    locales = {},
    i18nObjectAttributesPath = null,
    iconsFile = null,
    iconsPacks = {},
  }) {
    // Merge default translations with "locales" values
    if (Object.keys(locales).length > 0) Object.assign(translations, locales)

    // Attributes translations key in locales
    app.config.globalProperties.$_ruiI18nObjectAttributesPath = i18nObjectAttributesPath

    // Setup fallback $t method
    if (!useVueI18n) {
      app.config.globalProperties.$t = makeFakeT(translations, locale)
    }

    // Icons files. Fallbacks to the original icons file as default if none provided
    app.config.globalProperties.$_ruiIconsFiles = { rui: libIcons, default: iconsFile, ...iconsPacks }
  },
}

export const translations = locales
