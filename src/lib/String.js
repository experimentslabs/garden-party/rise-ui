const CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
const CHARS_LENGTH = CHARS.length

/**
 * Replaces all non-alphanumeric character by a dash
 *
 * @param   {string} string - String to parameterize
 * @returns {string}        Parameterized string
 */
export function parameterize (string) {
  return string.replace(/[\W\s_]/g, '-').toLowerCase()
}

/**
 * Returns a random string
 *
 * @param   {number} size - Output length
 * @returns {string}      A random string
 */
export function randomString (size = 5) {
  let result = ''
  for (let i = 0; i < size; i++) result += CHARS[Math.floor(Math.random() * CHARS_LENGTH)]

  return result
}
