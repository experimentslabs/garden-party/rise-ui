import { describe, expect, it } from 'vitest'

import { makeFakeT } from './FakeT.js'
import { translations } from '../library.js'

describe('makeFakeT', () => {
  const fakeT = makeFakeT(translations, 'en')

  it('returns a function', () => {
    expect(makeFakeT(translations, 'en')).toBeTypeOf('function')
  })

  describe('when the translation exists', () => {
    it('returns the string', () => {
      expect(fakeT('rui.confirm_dialog.are_you_sure')).toEqual('Are you sure?')
    })
  })

  describe('when the translation does not exist', () => {
    it('returns the given path', () => {
      const path = 'non.existant.path'

      expect(fakeT(path)).toEqual(path)
    })
  })
})
