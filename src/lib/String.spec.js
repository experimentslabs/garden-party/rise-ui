import { describe, expect, it } from 'vitest'
import * as StringHelper from './String.js'

describe('parameterize', () => {
  it('replaces characters', () => {
    expect(StringHelper.parameterize('%%a String_And#1 number')).toEqual('--a-string-and-1-number')
  })
})

describe('randomString', () => {
  it('does not create the same string two times', () => {
    const string = StringHelper.randomString()
    const otherString = StringHelper.randomString()
    expect(string).not.toEqual(otherString)
  })

  it('generates strings of custom lengths', () => {
    const string = StringHelper.randomString(30)
    expect(string).toHaveLength(30)
  })
})
