/**
 * Returns a random string
 *
 * @param   {number} size - Output length
 * @returns {string}      A random string
 */
export default function randomString (size = 5) {
  return Math.random().toString(36).substr(2, size)
}
