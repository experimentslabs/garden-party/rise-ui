/**
 * Generates a fallback $t method to be used instead of vueI18n
 *
 * @param   {object}                            translations - Translations object
 * @param   {string}                            locale       - Current locale
 * @returns {function({string}): string|object}              - Fallback $t method using current locale
 */
export function makeFakeT (translations, locale) {
  return function (path) {
    let current = translations[locale]

    path.split('.').forEach((key) => {
      if (!current) return
      current = current[key]
    })

    return current || path
  }
}
