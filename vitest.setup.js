import * as VueTestUtils from '@vue/test-utils'

VueTestUtils.config.global.mocks.$_ruiIconsFiles = { default: 'some/path' }
VueTestUtils.config.global.mocks.$t = (v) => v
