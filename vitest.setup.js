import * as VueTestUtils from '@vue/test-utils'

VueTestUtils.config.global.mocks.$_ruiIconsFile = 'some/path'
VueTestUtils.config.global.mocks.$t = (v) => v
