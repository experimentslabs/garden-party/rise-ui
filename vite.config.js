import path from 'path'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  build: {
    cssCodeSplit: true,
    lib: {
      entry: [
        path.resolve(__dirname, 'src/stylesheets/fonts.scss'),
        path.resolve(__dirname, 'src/stylesheets/full.scss'),
        path.resolve(__dirname, 'src/stylesheets/minimal.scss'),
        path.resolve(__dirname, 'src/stylesheets/rise_ui.scss'),
        path.resolve(__dirname, 'src/stylesheets/with_css_components.scss'),
      ],
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        api: 'modern',
      },
    },
  },
  test: {
    setupFiles: ['./vitest.setup.js'],
    environment: 'jsdom',
    restoreMocks: true,
    coverage: {
      all: true,
      exclude: [
        'docs/',
        'documentation_system/',
        'src/locales',
        'src/library.js',
        'src/components/index.js',
      ],
    },
  },
})
