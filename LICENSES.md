# Other licenses

## From [Phosphor Icons](https://phosphoricons.com/):
- src/assets/images/icons.svg; every `symbol` in it.
- src/sources/select-arrow.svg (whole file; defined as `caret-down` icon in Phosphor Icons list)

These icons are under the MIT license,
[as defined on the project's repository](https://github.com/phosphor-icons/homepage/blob/master/LICENSE).
