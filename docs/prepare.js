#!/usr/bin/env node

/**
 * Generates documentation and routes
 */

import path from 'path'
import { globSync } from 'glob'
import fs from 'fs'
import VueDocgenApi from 'vue-docgen-api'
import { fileURLToPath } from 'url'

import { Metadata } from './src/utils/generator/Metadata.js'

const __filename = fileURLToPath(import.meta.url)
const _filenameChunks = __filename.split(path.sep)
_filenameChunks.pop()
const __dirname = _filenameChunks.join(path.sep)

/**
 * Extracts documentation from components
 *
 * @returns {Promise<object>} Documentation for every component
 */
async function extractDocumentation () {
  const basePath = path.resolve(__dirname, '../src/components')
  const pattern = `${basePath}/**/*.vue`
  const files = globSync(pattern)

  const promises = []

  const list = []
  files.forEach((file) => {
    const promise = VueDocgenApi.parse(file)
      .then((docs) => {
        // Replace absolute path
        docs.sourceFiles = docs.sourceFiles.map(f => f.replace(`${basePath}/`, 'src/components/'))

        list.push(docs)
      })

    promises.push(promise)
  })

  await Promise.all(promises)

  return list
}

/**
 * Lists all documentation examples
 *
 * @returns {Promise<object>} List of examples per component
 */
async function listExamples () {
  const basePath = path.resolve(__dirname, 'src/examples')
  const pattern = `${basePath}/**/*.{vue,html}`
  const regex = new RegExp(`^${basePath}/`)

  const list = {}
  new globSync(pattern) // eslint-disable-line new-cap
    .sort()
    .forEach((f) => {
      const filePath = f.replace(regex, '')
      const chunks = filePath.split(path.sep)
      const component = chunks.shift()
      if (!list[component]) list[component] = { html: [], vue: [] }

      const exampleType = filePath.match(/\.vue$/) ? 'vue' : 'html'

      list[component][exampleType].push(filePath.replace(/\.(vue|html)$/, ''))
    })

  return list
}

const docs = await extractDocumentation()
const examples = await listExamples()

const output = Metadata.mix(docs, examples)

Metadata.check(output)

fs.writeFileSync(path.resolve(__dirname, 'src/documentation.json'), JSON.stringify(output))
