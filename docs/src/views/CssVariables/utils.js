export const sampleText = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'

/**
 * Replaces elements with the value extracted from current style
 */
export function setScssVariables () {
  document.querySelectorAll('[data-css-var]')
    .forEach(e => {
      e.innerHTML = getComputedStyle(e).getPropertyValue(e.dataset.cssVar)
    })
}
