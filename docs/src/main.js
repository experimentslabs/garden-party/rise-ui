import { DocumentationSystem } from '../../documentation_system/plugins/DocumentationSystem.js'
import { createApp } from 'vue/dist/vue.esm-bundler.js'

// All RiseUI components
import * as ruiComponents from '../../src/components/index.js'
// RiseUI extra components
import RuiHelpButton from '../../src/components/HelpButton/HelpButton.vue'
import RuiTagSelector from '../../src/components/Inputs/TagSelector/TagSelector.vue'
import RuiRgbaPicker from '../../src/components/Inputs/RgbaPicker/RgbaPicker.vue'
import RuiSearchForm from '../../src/components/Form/SearchForm.vue'

import documentation from './documentation.json'
import routes from './router'
import menu from './menu.js'
import { loadHTMLExample, loadMarkdownFile, loadVueExample, loadVueExampleSource, projectDefinition as project } from '../config.js'

import App from './App.vue'

const app = createApp(App)

app.use(DocumentationSystem, {
  documentation,
  routes,
  menu,
  project,
  loadHTMLExample,
  loadVueExample,
  loadVueExampleSource,
  loadMarkdownFile,
})

// Register all the base components
for (const name in ruiComponents) { app.component(name, ruiComponents[name]) }
// Register extra components
const riseUIExtraComponents = {
  RuiHelpButton,
  RuiTagSelector,
  RuiRgbaPicker,
  RuiSearchForm,
}
for (const name in riseUIExtraComponents) { app.component(name, riseUIExtraComponents[name]) }

app.mount('#app')
