import { createApp } from 'vue/dist/vue.esm-bundler.js'
import { createI18n } from 'vue-i18n'
import App from './App.vue'
import router from './router'

import { RiseUI, translations } from '../../src/library'
import iconsFile from '../../src/assets/images/icons.svg'

import { SetTitle } from './plugins/SetTitle.js'
import { Documentation } from './plugins/Documentation.js'

// Documentation UI
import './styles.scss'

const app = createApp(App)

app.use(router)
  .use(SetTitle)
  .use(Documentation)
  .use(createI18n({ locale: 'en', messages: translations }))
  .use(RiseUI, {
    iconsFile,
    useDebounce: true,
    usePopper: true,
    useVanillaPicker: true,
    useVueI18n: true,
    useVueMultiselect: true,
  })

app.mount('#app')
