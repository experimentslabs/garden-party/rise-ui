/**
 * @typedef {import('vue').App} VueApp
 */

export const SetTitle = {
  /**
   * @param {VueApp} app - Application
   */
  install (app) {
    app.config.globalProperties.$pageTitle = 'RiseUI'

    app.config.globalProperties.$setTitle = (string) => {
      const newTitle = `${string} - RiseUI`
      document.querySelector('head > title').innerHTML = newTitle
      app.config.globalProperties.$pageTitle = newTitle
    }
  },
}
