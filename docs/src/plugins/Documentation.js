import documentation from '../documentation.json'

class DocumentationLibrary {
  constructor (docs) {
    this._docs = docs
  }

  getComponent (name) {
    if (!Object.prototype.hasOwnProperty.call(this._docs, name)) throw new Error(`Unknown component ${name}`)

    return this._docs[name]
  }

  getExample (name) {
    return Object.keys(this._docs).forEach((k) => {
      const example = this._docs[k].examples[name]
      if (example) return example
    })
  }

  get componentNames () {
    return Object.keys(this._docs)
  }
}

export const Documentation = {
  install (app) {
    app.config.globalProperties.$docs = new DocumentationLibrary(documentation)
    app.config.globalProperties.$getDoc = function (name) { return this.$docs.getComponent(name) }
    app.config.globalProperties.$getExample = function (name) { return this.$docs.getExample(name) }
  },
}
