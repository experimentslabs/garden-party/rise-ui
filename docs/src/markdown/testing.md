**Note:** This was tested with `vitest`, but should also work with `jest`.

## Stubbing

If you don't need to test the behavior/state of the RiseUI components in your tests, stub them:

```js
const wrapper = mount(SomeComponent, {
  global: {
    // As an array of components:
    stubs: [ 'RuiIcon', 'RuiButton' ],
    // Or as an an object
    stubs: {
      RuiIcon: true,
      RuiButton: SomeCustomStub
    },
  },
  //...
})
```

## Rendering

If you need to test the behavior of a RiseUI component in the test and don't want to stub it, you need to import the
component:

```js
import { RuiButton, RuiIcon } from '@experiments-labs/rise_ui/components'

const wrapper = mount(SomeComponent, {
  components: { RuiButton, RuiIcon }
  //...
})
```
