## Icons configuration for VueJS

RiseUI comes with a few base icons, usable with prefix `rui:`:
```html
<rui-icon name="rui:asterisk" />
```

Icons are organized in _packs_ internally; these are defined during plugin configuration:

```js
import myIcons from 'path/to/your/icons.svg'
import otherIcons from 'path/to/your/other-icons.svg'
import ruiIconsOverrides from 'path/to/your/overrides.svg'

app.use(RiseUi, {
  // Longer declaration
  iconsPacks: {
    // Default pack, available with no name prefix
    default: myIcons,
    // Available with name "custom:*"
    custom: otherIcons,
    // Re-define icon pack for Rise UI components.
    // the override file should contain all the icons available on RiseUI (see the icons reference)
    rui: ruiIconsOverrides
  }
})
```

If you only want to define a default icons pack, use the shorter option `iconsFile`:

```js
import myIcons from 'path/to/your/icons.svg'

app.use(RiseUi, {
  // Short declaration
  iconsFile: myIcons,
})
```

The SVG icon files should use the expected format. See below.

## Icons file format

The framework expects icons to be a SVG file with symbols. An example file should be like:

```svg
<!-- IDs are prefixed with "icon-"; icon name follows. -->
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <!-- Empty icon; use it as a spacer -->
  <symbol id="icon-blank" viewBox="0 0 256 256"/>

  <!-- Playground icon -->
  <symbol id="icon-playground" viewBox="0 0 256 256">
    <path d="M221.69,199.77,160,96.92V40h8a8,8,0,0,0,0-16H88a8,8,0,0,0,0,16h8V96.92L34.31,199.77A16,16,0,0,0,48,224H208a16,16,0,0,0,13.72-24.23ZM110.86,103.25A7.93,7.93,0,0,0,112,99.14V40h32V99.14a7.93,7.93,0,0,0,1.14,4.11L183.36,167c-12,2.37-29.07,1.37-51.75-10.11-15.91-8.05-31.05-12.32-45.22-12.81ZM48,208l28.54-47.58c14.25-1.74,30.31,1.85,47.82,10.72,19,9.61,35,12.88,48,12.88a69.89,69.89,0,0,0,19.55-2.7L208,208Z"/>
  </symbol>
</svg>
```

## Using icons outside Vue

To display an icon, an SVG element should be used, like:

```html
<svg class="rui-icon" viewBox="0 0 24 24">
  <use xlink:href="path/to/your/icons.svg#icon-circle-edit"></use>
</svg>
```
