[[toc]]

## Rails helper

As an example, we use something similar in Garden Party:

```rb
# app/helpers/rise_ui_helper.rb

module RiseUiHelper
  # Displays an icon
  #
  # @param name  [String]  Icon name
  # @param size  [String]  Size: 3x, 4x or 10x
  # @param spin  [Boolean] Apply the "spin" variant
  # @param title [String]  HTML title attribute
  #
  def rui_icon(name, size: nil, spin: false, title: nil)
    title = title ? "<title>#{title}</title>" : ''
    classes = 'rui-icon'
    classes += ' rui-icon--spin' if spin
    classes += " rui-icon--#{size}x" if size

    # We use `String.html_safe` here as we NEVER generate icons based on user inputs. You may have a different use-case.
    <<~HTML.html_safe # rubocop:disable Rails/OutputSafety
      <svg class="#{classes}" viewBox="0 0 24 24">
        #{title}
        <use xlink:href="#{asset_url 'icons.svg'}#icon-#{name}" />
      </svg>
    HTML
  end
end
```

## Icons file generator

When developing Garden Party, we used a script to combine icons from multiple sources.

### The icons reference

```yaml
# config/icons.yml

---
# Icon configuration
# An SVG file is generated using "rake icons: generate" and used in the site.

# Source SVGs
paths:
  ph: node_modules/phosphor-icons/assets/
  gp: src/icons
output: app/assets/images/icons.svg
# SVG symbol prefix
prefix: icon-
# Fallback viewbox when none is defined in source. When this value is used,
# it may break the incomplete icon.
viewbox: 0 0 20 20

# Icon list. The key is the name to use. The value is the source to the path.
icons:
  # Special
  blank: gp:blank

  # Drawing
  circle-edit: gp:circle-edit
  circle-plus: gp:circle-plus
  #...

  # Layout
  activity: ph:regular/clock-counter-clockwise
  archive: ph:regular/archive
  #...
  x-square: ph:regular/x-square
```

### Rake task

```rb
# lib/tasks/icons.rake

require 'nokogiri'

CONFIGURATION_FILE = Rails.root.join('config', 'icons.yml')
CONFIG             = YAML.load_file CONFIGURATION_FILE
SVG_STRING         = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  width="0" height="0" style="display:none;"/>'.freeze

namespace :icons do
  desc 'Generate icon font defined in config/icons.yml'
  task :generate, [:output] => :environment do |_task, args|
    output = ENV.fetch('OUTPUT', nil) || args[:output].presence || Rails.root.join(CONFIG['output'])

    # Prepare definition file content
    svg_definitions    = Nokogiri::XML.fragment SVG_STRING
    xml_namespace      = svg_definitions.children.first.namespace.href
    defs_node          = svg_definitions.xpath('./ns:svg', ns: xml_namespace).first
    defs_node['class'] = 'icons-definition'

    CONFIG['icons'].each do |name, path|
      source_path = icon_path path

      svg = Nokogiri::XML.fragment File.read(source_path)
      svg_viewbox = svg.xpath('./ns:svg', ns: xml_namespace).attribute('viewBox')&.value
      svg_content = svg.xpath('./ns:svg/*', ns: xml_namespace)
      defs_node.add_child create_icon_def(name, svg_content, svg_viewbox)
    end

    File.write(output, svg_definitions)
  end

  def icon_path(icon)
    chunks = icon.split(':')
    path = Rails.root.join CONFIG['paths'][chunks.first], "#{chunks.last}.svg"
    raise "Missing icon at #{path}" unless File.exist? path

    path
  end

  def create_icon_def(name, content, svg_viewbox)
    unless svg_viewbox
      svg_viewbox = CONFIG['viewbox']
      puts "No viewbox provided for #{name}. Using '#{svg_viewbox}'"
    end

    id       = "#{CONFIG['prefix']}#{name}"
    fragment = Nokogiri::XML.fragment "<symbol id='#{id}' viewBox='#{svg_viewbox}'/>"
    fragment.xpath('./symbol').first.add_child content
    fragment
  end
end
```

### Gitlab CI job

Check if icons were generated and commited:

```yaml
# .gitlab-ci.yml

---
#...
generated_files:
  stage: test
  script:
    # Dependencies should be installed at this point
    - bundle exec rake icons:generate
    - git diff
    - git diff-index --quiet HEAD
```
