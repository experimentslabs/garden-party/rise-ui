# Document your own components

You can use _this_ documentation system to document your own custom components based on the RiseUI library, instead of
a more generic system like Storybook.

It integrates well but needs some configuration, mainly due to technical reasons/limitations.

**Until there is a better documentation written for this feature, you can still check some working implementations:**

- [Documentation of this project](https://gitlab.com/experimentslabs/garden-party/rise-ui/-/tree/develop/docs)
- [Garden Party technical documentation](https://gitlab.com/experimentslabs/garden-party/garden-party/-/tree/develop/docs)
