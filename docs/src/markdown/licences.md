# Licences

The library is licenced under the [MIT licence](https://mit-license.org/); licence file is included in the root of this
project.

Check what you can/cannot do on [TL;DR](https://www.tldrlegal.com/license/mit-license)

## Dependencies
Dependencies have their own licenses. You can check them individually in `package.json` files from `node_modules/`.

## Icons

Icons are taken from the [Phosphor Icons](https://phosphoricons.com/) package and licenced under the MIT license.

## Other

When code is copied from other sources, we tried our best to include a link and credit the author in code comments.
