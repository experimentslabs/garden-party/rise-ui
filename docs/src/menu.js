/**
 * @typedef {{name: ?string, params: ?object, to: ?string, caption: string}} MenuEntry
 * @typedef {{name: ?string, icon: ?string, children: MenuEntry[], collasible: ?boolean}} MenuGroup
 * @typedef {MenuGroup[]} MenuDefinition
 */

/** @type {MenuDefinition} */
export default [
  {
    children: [
      { name: 'readme', caption: 'Readme' },
      { name: 'changelog', caption: 'Changelog' },
      { name: 'markdown-page', params: { page: 'licences' }, caption: 'Licenses' },
      { name: 'code-of-conduct', caption: 'Code of conduct' },
      { name: 'contributing', caption: 'Contribution guide' },
      { name: 'migrating', caption: 'Migration guide' },
    ],
  },
  {
    name: 'Documents',
    icon: 'doc:library',
    collapsible: false,
    children: [
      { name: 'icons', caption: 'Icons' },
      { name: 'i18n', caption: 'I18n' },
      { name: 'testing', caption: 'Testing' },
      { name: 'libraries', caption: 'Libraries' },
      { name: 'markdown-page', params: { page: 'document-your-own-components' }, caption: 'Document your own components' },
    ],
  },
  {
    name: 'Html',
    icon: 'doc:file-css',
    children: [
      { name: 'reset', caption: 'Reset' },
    ],
  },
  {
    name: 'CSS variables',
    icon: 'doc:file-css',
    children: [
      { name: 'colors', caption: 'Colors' },
      { name: 'fonts', caption: 'Fonts' },
      { name: 'spacers', caption: 'Spacers' },
      { name: 'border-radius', caption: 'Border radius' },
      { name: 'z-indexes', caption: 'Z-indexes' },
      { name: 'components', caption: 'Components' },
    ],
  },
  {
    name: 'CSS helpers',
    icon: 'doc:file-css',
    children: [
      { name: 'accessibility', caption: 'Accessibility' },
      { name: 'background', caption: 'Background' },
      { name: 'borders', caption: 'Borders' },
      { name: 'grids-and-flex', caption: 'Grids/Flex' },
      { name: 'input', caption: 'Input' },
      { name: 'links', caption: 'Links' },
      { name: 'lists', caption: 'Lists' },
      { name: 'margins', caption: 'Margins' },
      { name: 'paddings', caption: 'Paddings' },
      { name: 'positions', caption: 'Positions' },
      { name: 'text', caption: 'Text' },
      { name: 'visibility', caption: 'Visibility' },
    ],
  },
]
