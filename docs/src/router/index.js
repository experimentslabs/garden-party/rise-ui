import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/readme',
      name: 'readme',
      component: () => import('../views/ReadmeView.vue'),
    },

    {
      path: '/changelog',
      name: 'changelog',
      component: () => import('../views/ChangelogView.vue'),
    },
    {
      path: '/licences',
      name: 'licences',
      component: () => import('../views/LicensesView.vue'),
    },

    {
      path: '/icons',
      name: 'icons',
      component: () => import('../views/IconsView.vue'),
    },
    {
      path: '/i18n',
      name: 'i18n',
      component: () => import('../views/I18nView.vue'),
    },
    {
      path: '/testing',
      name: 'testing',
      component: () => import('../views/TestingView.vue'),
    },
    {
      path: '/reset',
      name: 'reset',
      component: () => import('../views/HtmlResetView.vue'),
    },

    {
      path: '/css-variables',
      children: [
        { path: 'colors', name: 'colors', component: () => import('../views/CssVariables/ColorsView.vue') },
        { path: 'components', name: 'components', component: () => import('../views/CssVariables/ComponentsView.vue') },
        { path: 'fonts', name: 'fonts', component: () => import('../views/CssVariables/FontsView.vue') },
        { path: 'spacers', name: 'spacers', component: () => import('../views/CssVariables/SpacersView.vue') },
        { path: 'border-radius', name: 'border-radius', component: () => import('../views/CssVariables/BorderRadiusView.vue') },
        { path: 'z-indexes', name: 'z-indexes', component: () => import('../views/CssVariables/ZIndexesView.vue') },
      ],
    },

    {
      path: '/css-helpers',
      children: [
        { name: 'accessibility', path: 'accessibility', component: () => import('../views/CssHelpers/AccessibilityView.vue') },
        { name: 'background', path: 'background', component: () => import('../views/CssHelpers/BackgroundView.vue') },
        { name: 'flexbox', path: 'flexbox', component: () => import('../views/CssHelpers/FlexboxView.vue') },
        { name: 'grids', path: 'grids', component: () => import('../views/CssHelpers/GridsView.vue') },
        { name: 'input', path: 'input', component: () => import('../views/CssHelpers/InputView.vue') },
        { name: 'lists', path: 'lists', component: () => import('../views/CssHelpers/ListsView.vue') },
        { name: 'margins', path: 'margins', component: () => import('../views/CssHelpers/MarginsView.vue') },
        { name: 'paddings', path: 'paddings', component: () => import('../views/CssHelpers/PaddingsView.vue') },
        { name: 'text', path: 'text', component: () => import('../views/CssHelpers/TextView.vue') },
        { name: 'visibility', path: 'visibility', component: () => import('../views/CssHelpers/VisibilityView.vue') },
      ],
    },

    {
      path: '/docs/components/:page',
      name: 'doc',
      component: () => import('../views/DocumentView.vue'),
    },
  ],
})

export default router
