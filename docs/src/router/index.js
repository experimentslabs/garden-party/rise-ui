import HomeView from '../views/HomeView.vue'

export default [
  { path: '/', component: HomeView },
  { path: '/readme', name: 'readme', component: () => import('../views/ReadmeView.vue') },

  { path: '/changelog', name: 'changelog', component: () => import('../views/ChangelogView.vue') },
  { path: '/code-of-conduct', name: 'code-of-conduct', component: () => import('../views/CodeOfConductView.vue') },
  { path: '/contributing', name: 'contributing', component: () => import('../views/ContributionGuideView.vue') },
  { path: '/migrating', name: 'migrating', component: () => import('../views/MigrationView.vue') },

  { path: '/icons', name: 'icons', component: () => import('../views/IconsView.vue') },
  { path: '/i18n', name: 'i18n', component: () => import('../views/I18nView.vue') },
  { path: '/libraries', name: 'libraries', component: () => import('../views/LibrariesView.vue') },
  { path: '/testing', name: 'testing', component: () => import('../views/TestingView.vue') },
  { path: '/reset', name: 'reset', component: () => import('../views/HtmlResetView.vue') },

  {
    path: '/css-variables',
    children: [
      { path: 'colors', name: 'colors', component: () => import('../views/CssVariables/ColorsView.vue') },
      { path: 'components', name: 'components', component: () => import('../views/CssVariables/ComponentsView.vue') },
      { path: 'fonts', name: 'fonts', component: () => import('../views/CssVariables/FontsView.vue') },
      { path: 'spacers', name: 'spacers', component: () => import('../views/CssVariables/SpacersView.vue') },
      { path: 'border-radius', name: 'border-radius', component: () => import('../views/CssVariables/BorderRadiusView.vue') },
      { path: 'z-indexes', name: 'z-indexes', component: () => import('../views/CssVariables/ZIndexesView.vue') },
    ],
  },

  {
    path: '/css-helpers',
    children: [
      { name: 'accessibility', path: 'accessibility', component: () => import('../views/CssHelpers/AccessibilityView.vue') },
      { name: 'background', path: 'background', component: () => import('../views/CssHelpers/BackgroundView.vue') },
      { name: 'borders', path: 'borders', component: () => import('../views/CssHelpers/BordersView.vue') },
      { name: 'grids-and-flex', path: 'grids-and-flex', component: () => import('../views/CssHelpers/GridsAndFlexView.vue') },
      { name: 'input', path: 'input', component: () => import('../views/CssHelpers/InputView.vue') },
      { name: 'links', path: 'links', component: () => import('../views/CssHelpers/LinksView.vue') },
      { name: 'lists', path: 'lists', component: () => import('../views/CssHelpers/ListsView.vue') },
      { name: 'margins', path: 'margins', component: () => import('../views/CssHelpers/MarginsView.vue') },
      { name: 'paddings', path: 'paddings', component: () => import('../views/CssHelpers/PaddingsView.vue') },
      { name: 'positions', path: 'positions', component: () => import('../views/CssHelpers/PositionsView.vue') },
      { name: 'text', path: 'text', component: () => import('../views/CssHelpers/TextView.vue') },
      { name: 'visibility', path: 'visibility', component: () => import('../views/CssHelpers/VisibilityView.vue') },
    ],
  },
]
