export const sampleText = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
export const longSampleText = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aperiam commodi consequatur dolor dolores dolorum, error facere ipsa minima provident quis reiciendis sapiente soluta suscipit temporibus tenetur vitae voluptatibus voluptatum!'

// C.f.: src/stylesheets/core/_variables_scss.scss
export const sizes = [0, 't', 's', 'd', 'm', 'l', 'xl']
// C.f.: src/stylesheets/core/_variables_scss.scss
export const sides = {
  t: 'top',
  r: 'right',
  b: 'bottom',
  l: 'left',
}
// C.f.: src/stylesheets/core/_variables_scss.scss
export const colors = [
  'primary',
  'black-like',
  'white-like',
  'blue',
  'green',
  'mute',
  'orange',
  'red',
  'yellow',
  'olive',
  'teal',
  'violet',
  'purple',
  'pink',
  'brown',
  'grey',
]

// From any theme file: --state-color-*
export const states = [
  'mute',

  'danger',
  'error',
  'destructive',

  'helping',
  'creative',

  'done',
  'success',
  'recording',

  'warning',
  'canceling',
]

// C.f.: src/stylesheets/core/_variables_css.scss: --font-weight-*
export const weights = [
  'light',
  'base',
  'medium',
  'bold',
]
