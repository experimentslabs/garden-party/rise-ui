import { Logger } from './Logger.js'

/**
 * Helper class to merge and generate component documentation
 *
 * Resulting object is a mix of `vue-docgen-api` output and other data
 */

/**
 * @typedef {import('vue-docgen-api').ComponentDoc} ComponentDoc
 * @typedef {import('vue-docgen-api').EventDescriptor} EventDescriptor
 * @typedef {import('vue-docgen-api').PropDescriptor} PropDescriptor
 * @typedef {import('vue-docgen-api').BlockTag} BlockTag
 * @typedef {import('vue-docgen-api').SlotDescriptor} SlotDescriptor
 */

/**
 * @typedef {object} DocumentProp
 * @property {string}   name         - Prop name
 * @property {string}   description  - Prop description
 * @property {object}   type         - Prop type description
 * @property {boolean}  required     - Whether this prop is required
 * @property {object}   defaultValue - Default value description
 * @property {string[]} values       - Array of possible values
 * @property {boolean}  deprecated   - Whether the usage of this prop should be avoided
 */

/**
 * @typedef {object} Examples
 * @property {string[]} html - List of HTML examples
 * @property {string[]} vue  - List of VueJS examples
 */

/**
 * @typedef {object} PlaygroundPreset
 * @property {{[key: string]: object}} props - Props values
 * @property {{[key: string]: string}} slots - Slots content
 */

/**
 * @typedef {object} Playground
 * @property {boolean}                           enabled       - Whether to enable the playground
 * @property {{[key: string]: PlaygroundPreset}} presets       - List of presets
 * @property {string[]}                          disabledProps - List of props to disable in the playground
 */

/**
 * @typedef {object} DocumentStructure
 * @property {string}            displayName - Component's full name
 * @property {?string}           description - Description
 * @property {boolean}           deprecated  - Whether the component should be avoided
 * @property {boolean}           vuejs       - Whether the component is a VueJS component
 * @property {boolean}           html        - Whether the component can be used with only HTML
 * @property {DocumentProp[]}    props       - Component props
 * @property {SlotDescriptor[]}  slots       - Component slots
 * @property {EventDescriptor[]} events      - Component events
 * @property {string[]}          sourceFiles - List of source files
 * @property {Examples}          examples    - Lists of html and VueJS examples
 * @property {Playground}        playground  - Playground configuration
 */

/**
 * @typedef {{[key: string]: DocumentStructure}} CompletedDocumentation
 */

export class Metadata {
  /**
   * Completes and augments documentation by merging components an examples
   *
   * @param   {ComponentDoc[]}            documentation - Documentation from vue-docgen-api
   * @param   {{[key: string]: Examples}} examples      - List of examples per component.
   * @returns {CompletedDocumentation}                  - Completed documentation
   */
  static mix (documentation, examples) {
    const obj = {}

    // Process all VueJS components
    for (const doc of documentation) {
      obj[doc.displayName] = this._processVueComponent(doc, examples[doc.displayName])
    }

    Object.keys(examples).forEach((name) => {
      // Skip examples that already are in a Vue component as they are already processed
      if (obj[name]) return

      /* eslint-disable no-multi-spaces */
      obj[name] = this._document()
      obj[name].displayName   = name
      obj[name].html          = true
      obj[name].examples.html = examples[name].html
      /* eslint-enable no-multi-spaces */
    })

    return obj
  }

  /**
   * Checks various points in metadata and displays warnings
   *
   * @param {CompletedDocumentation} docs - Complete metadata (output of `Metadata.mix()`)
   */
  static check (docs) {
    Object.keys(docs).forEach((name) => {
      const doc = docs[name]
      const errors = [
        ...this._checkComponentDescription(doc),
        ...this._checkPropsDescription(doc),
        ...this._checkPlaygroundAndExamples(doc),
        ...this._checkValidIgnoredProps(doc),
        ...this._checkValidPropsAndSlotsInPresets(doc),
      ]
      if (errors.length > 0) {
        Logger.error(name)
        Logger.warn(`  - ${errors.join('\n  - ')}`)
      }
    })
  }

  /**
   * Returns the value of a tag.
   *
   * If the tag has multiple values, returns the first.
   * Without a fallback, an error will be thrown if the tag is missing.
   *
   * @param   {PropDescriptor|ComponentDoc} item     - Item to search in
   * @param   {string}                      tag      - Tag to search
   * @param   {*}                           fallback - Fallback value if tog is not found
   * @returns {*}                                    - Fallback or tag value
   * @private
   */
  static _singleTagValue (item, tag, fallback) {
    if (!this._hasTag(item, tag)) {
      if (fallback === undefined) throw new Error('Trying to access tag on an object without tags')

      return fallback
    }

    return item.tags[tag][0].description
  }

  /**
   * Checks if an item has a description for the given tag
   *
   * @param   {PropDescriptor|ComponentDoc} item - Item to search in
   * @param   {string}                      tag  - Tag to search
   *
   * @returns {boolean}                          Tag presence
   * @private
   */
  static _hasTag (item, tag) {
    return !!(item && item.tags && Object.hasOwnProperty.call(item.tags, tag))
  }

  /**
   * Completes a VueJS component documentation
   *
   * @param   {ComponentDoc}      documentation - Component documentation
   * @param   {Examples}          examples      - Associated examples
   * @returns {DocumentStructure}               Completed documentation
   * @private
   */
  static _processVueComponent (documentation, examples) {
    const obj = this._document()

    /* eslint-disable no-multi-spaces */
    obj.displayName = documentation.displayName
    obj.description = documentation.description
    obj.deprecated =  this._singleTagValue(documentation, 'deprecated', false)
    obj.vuejs =       true
    obj.html =        (examples?.html || []).length > 0
    obj.props =       (documentation?.props || []).map(p => this._processProp(p))
    obj.slots =       documentation.slots || []
    obj.events =      documentation.events || []
    obj.sourceFiles = documentation.sourceFiles
    obj.playground =  {
      enabled: !this._singleTagValue(documentation, 'noPlayground', false),
      presets: this._presets(documentation),
      disabledProps: this._singleTagValue(documentation, 'disableProps', '').split(',').map(v => v.trim()).filter(v => v !== ''),
    }
    /* eslint-enable no-multi-spaces */

    if (examples) obj.examples = examples

    // Custom tags: bindValue.
    // This is the only object entry we add only if it has the tag.
    // FIXME: assign an object instead, like {bindValue: {active: true, value: xyz}} so it's defined
    if (Object.hasOwnProperty.call(documentation.tags, 'bindValue')) {
      obj.playground.bindValue = documentation.tags?.bindValue[0].description
    }

    return obj
  }

  /**
   * Returns an empty document structure
   *
   * @returns {DocumentStructure} - Empty structure
   * @private
   */
  static _document () {
    return {
      displayName: null,
      description: null,
      deprecated: false,
      vuejs: false,
      html: false,
      props: [],
      slots: [],
      events: [],
      sourceFiles: [],
      examples: { html: [], vue: [] },
      playground: { enabled: false, presets: {}, disabledProps: [] },
    }
  }

  /**
   * Completes a prop
   *
   * @param   {PropDescriptor} prop - Incomplete prop documentation
   * @returns {DocumentProp}        Completed prop
   * @private
   */
  static _processProp (prop) {
    /* eslint-disable key-spacing */
    return {
      name:         prop.name,
      description:  prop.description,
      type:         prop.type,
      required:     prop.required,
      defaultValue: prop.defaultValue,
      values:       prop.values || [],
      deprecated:   this._singleTagValue(prop, 'deprecated', false),
    }
    /* eslint-enable key-spacing */
  }

  /**
   * Checks that components have at least an example or an active playground
   *
   * @param   {object}   doc - Complete metadata for a component
   * @returns {string[]}     Errors
   * @private
   */
  static _checkPlaygroundAndExamples (doc) {
    const errors = []
    if (
      !doc.playground.enabled
      && doc.examples.vue.length + doc.examples.html.length === 0
      && !doc.internal) {
      errors.push(`${doc.displayName}: has no playground and no example`)
    }

    return errors
  }

  /**
   * Checks that component have at least an example or an active playground
   *
   * @param   {object}   doc - Complete metadata for a component
   * @returns {string[]}     Errors
   * @private
   */
  static _checkValidIgnoredProps (doc) {
    const errors = []
    const props = doc.props ? doc.props.map(e => e.name) : []

    if (Object.hasOwnProperty.call(doc.playground, 'disabledProps')) {
      doc.playground.disabledProps.forEach((p) => {
        if (props.includes(p)) return

        errors.push(`${doc.displayName}: disables inexistant prop "${p}"`)
      })
    }

    return errors
  }

  /**
   * Checks that component have valid props and slots in their presets
   *
   * @param   {object}   doc - Complete metadata for a component
   * @returns {string[]}     Errors
   * @private
   */
  static _checkValidPropsAndSlotsInPresets (doc) {
    const errors = []

    const props = doc.props ? doc.props.map(e => e.name) : []
    const slots = doc.slots ? doc.slots.map(e => e.name) : []

    Object.keys(doc.playground.presets).forEach((name) => {
      const preset = doc.playground.presets[name]

      Object.keys(preset?.props || {}).forEach(p => {
        if (props.includes(p)) return
        errors.push(`preset "${name}": uses inexistant prop "${p}"`)
      })

      Object.keys(preset?.slots || {}).forEach(p => {
        if (slots.includes(p)) return
        errors.push(`preset "${name}": uses inexistant slot "${p}"`)
      })
    })

    return errors
  }

  /**
   * Checks that a component has a description
   *
   * @param   {object}   doc - Complete metadata for a component
   * @returns {string[]}     Errors
   * @private
   */
  static _checkComponentDescription (doc) {
    if (!doc.vuejs || doc.description) return []
    return ['has no description']
  }

  /**
   * Checks that all props in a component have descriptions
   *
   * @param   {object}   doc - Complete metadata for a component
   * @returns {string[]}     Errors
   * @private
   */
  static _checkPropsDescription (doc) {
    const errors = []
    doc.props.forEach(prop => {
      if (['modelValue', 'required'].includes(prop.name)) return

      if (!prop.description || prop.description === '') errors.push(`prop ${prop.name} has no description`)
    })

    return errors
  }

  /**
   * Extracts playground presets from tags
   *
   * @param   {ComponentDoc}                      component - Tags from "vue-docgen-api"
   * @returns {{[key: string]: PlaygroundPreset}}           - Presets
   *
   * @private
   */
  static _presets (component) {
    if (!this._hasTag(component, 'preset')) return { default: { props: {}, slots: {} } }

    const presets = {}
    component.tags.preset.forEach((p) => {
      const matches = p.description.match(/^\s*(?<name>\S+)\s*(?<json>[\s\S]*)$/)
      if (!matches) throw new Error(`Malformed preset: ${p.description}`)

      let preset
      const name = matches.groups.name

      try {
        preset = JSON.parse(matches.groups.json)
      } catch (e) {
        throw new Error(`Invalid JSON for preset "${name}": ${matches.groups.json}`)
      }

      if (!preset.props) preset.props = {}
      if (!preset.slots) preset.slots = {}

      presets[name] = preset
    })

    return presets
  }
}
