import { sizes } from './constants.js'

/**
 * Generates a list of concatenated sizes.
 *
 * This helper is used to generate size variants.
 *
 * Example:
 * ```js
 * sizeVariant('-h-')
 * // ['-h-t', '-h-s', ... '-h-xl']
 * ```js
 *
 * @param   {string}   base - String to concatenate
 * @returns {string[]}      The list
 */
export function sizeVariant (base) { return sizes.map(s => `${base}${s}`) }
