import libPackage from '../package.json'

/**
 * @typedef ProjectDefinition
 * @property {string?}     repositoryUrl - Repository URL
 * @property {string?}     version       - Project version
 * @property {string|null} name          - Project name
 */

/** @type {ProjectDefinition} */
export const projectDefinition = {
  name: 'RiseUI',
  version: libPackage.version,
  repositoryUrl: 'https://gitlab.com/experimentslabs/garden-party/rise-ui',
}

/**
 * Loads an HTML example file
 *
 * @param   {string}     path - Example path, like "MyComponent/MyComponent.example.default"
 * @returns {Promise<*>}      File loading promise
 */
export function loadHTMLExample (path) {
  const chunks = path.split('/')

  return import(`./src/examples/${chunks[0]}/${chunks[1]}.html?raw`)
}

/**
 * Loads a VueJS example file
 *
 * @param   {string}     path - Example path, like "MyComponent/MyComponent.example.default"
 * @returns {Promise<*>}      File loading promise
 */
export function loadVueExample (path) {
  const chunks = path.split('/')

  return import(`./src/examples/${chunks[0]}/${chunks[1]}.vue`)
}

/**
 * Loads content of a VueJS example file
 *
 * @param   {string}     path - Example path, like "MyComponent/MyComponent.example.default"
 * @returns {Promise<*>}      File loading promise
 */
export function loadVueExampleSource (path) {
  const chunks = path.split('/')

  return import(`./src/examples/${chunks[0]}/${chunks[1]}.vue?raw`)
}

/**
 * Loads content a markdown file
 *
 * @param   {string}     name - File name, without extension
 * @returns {Promise<*>}      File loading promise
 */
export function loadMarkdownFile (name) {
  return import(`./src/markdown/${name}.md?raw`)
}
