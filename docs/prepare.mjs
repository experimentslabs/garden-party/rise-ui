#!/usr/bin/env node

import path from 'path'
import { generateMetadata } from '../documentation_system/utils/generator/generator.js'
import { fileURLToPath } from 'url'

await generateMetadata(
  {
    docsAppPath: path.dirname(fileURLToPath(import.meta.url)), // Current directory
    relativeComponentsPath: '../src/components',
    ignoredComponents: [
      'VueMultiselect',
    ],
  })
